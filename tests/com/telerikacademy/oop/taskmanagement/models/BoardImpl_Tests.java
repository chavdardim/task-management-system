package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.models.BoardImpl;
import com.telerikacademy.oop.managementsystem.models.BugImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.managementsystem.models.BoardImpl.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class BoardImpl_Tests {

    @Test
    public void constructor_should_throwException_when_nameLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BoardImpl(initializeStringWithSize(NAME_MIN_LENGTH-1)));
    }

    @Test
    public void addTask_should_addTask_when_inputIsValid() {
        //Arrange
        Task task = createBug();
        Board board = createBoard();
        createTeam().addBoard(board);
        board.addTask(task);

        //Act, Assert
        Assertions.assertEquals(1, board.getTasks().size());
    }

    @Test
    public void removeTask_should_removeTask_when_inputIsValid() {
        //Arrange
        Task task = createBug();
        Board board = createBoard();
        Team team = createTeam();
        team.addBoard(board);
        board.addTask(task);
        board.removeTask(task);

        //Act, Assert
        Assertions.assertEquals(0, board.getTasks().size());
    }
}
