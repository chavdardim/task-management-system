package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.models.BoardImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.managementsystem.models.TeamImpl.NAME_MIN_LENGTH;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.createTeam;

public class TeamImpl_Tests {
    @Test
    public void constructor_should_throwException_when_nameLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BoardImpl(initializeStringWithSize(NAME_MIN_LENGTH-1)));
    }

    @Test
    public void addBoard_should_addBoard_when_inputIsValid() {
        //Arrange
        Team team = createTeam();
        Board board = createBoard();
        team.addBoard(board);

        //Act, Assert
        Assertions.assertEquals(1, team.getBoards().size());
    }

    @Test
    public void addMember_should_addMember_when_inputIsValid() {
        //Arrange
        Team team = createTeam();
        Member member = createMember();
        team.addMember(member);

        //Act, Assert
        Assertions.assertEquals(1, team.getMemberList().size());
    }
}
