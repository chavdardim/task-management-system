package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.models.BugImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.managementsystem.models.BugImpl.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class TaskBaseImpl_Tests {

    @Test
    public void constructor_should_throwException_when_titleLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BugImpl(
                        1,
                        initializeStringWithSize(TITLE_MIN_LENGTH-1),
                        BUG_VALID_DESCRIPTION));
    }

    @Test
    public void constructor_should_throwException_when_descriptionLessThanMinimum() {
        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> new BugImpl(
                        1,
                        BUG_VALID_TITLE,
                        initializeStringWithSize(DESCRIPTION_MIN_LENGTH-1)
                ));
    }

    @Test
    public void addComment_should_throwException_when_commentLessThanMinimum() {
        Task task = createBug();
        String invalidComment = initializeStringWithSize(COMMENT_MIN_LENGTH-1);

        //Arrange, Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> task.addComment(invalidComment));
    }
}
