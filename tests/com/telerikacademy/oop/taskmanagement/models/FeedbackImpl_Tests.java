package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.StorySize;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.createFeedback;

public class FeedbackImpl_Tests {
    @Test
    public void changePriority_throwsException_whenThePriorityIsTheSame() {
        // Arrange
        Feedback feedback = createFeedback();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->
                feedback.changeRating(1));
    }

    @Test
    public void changeSeverity_throwsException_whenTheSeverityIsTheSame() {
        // Arrange
        Feedback feedback = createFeedback();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->
                feedback.changeFeedbackStatus(FeedbackStatus.NEW));
    }
}
