package com.telerikacademy.oop.taskmanagement.models;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.createStory;

public class StoryImpl_Tests {
    @Test
    public void changePriority_throwsException_whenThePriorityIsTheSame() {
        // Arrange
        Story story = createStory();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->
                story.changePriority(Priority.LOW));
    }

    @Test
    public void changeSeverity_throwsException_whenTheSeverityIsTheSame() {
        // Arrange
        Story story = createStory();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->
                story.changeStorySize(StorySize.SMALL));
    }

    @Test
    public void changeBugStatus_throwsException_whenTheBugStatusIsTheSame() {
        // Arrange
        Story story = createStory();

        // Act, Assert
        Assertions.assertThrows(InvalidUserInputException.class,() ->
                story.changeStoryStatus(StoryStatus.NOTDONE));
    }
}
