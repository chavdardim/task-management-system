package com.telerikacademy.oop.taskmanagement.core;

import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Team.*;
import static org.junit.jupiter.api.Assertions.*;

public class ManagementSystemRepositoryImpl_Tests {

    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    void setUp() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
    }

    @Test
    void memberExists_should_returnTrue_when_exists() {
        Member member = initializeMember(managementSystemRepository);

        Assertions.assertTrue(managementSystemRepository.memberExists(member.getName()));
    }

    @Test
    void memberExists_should_returnFalse_when_doesntExist() {
        Assertions.assertFalse(managementSystemRepository.memberExists("company-name"));
    }

    @Test
    void addPerson_should_addPerson() {
        Member person = initializePerson(managementSystemRepository);

        assertTrue(managementSystemRepository.personExists(person.getName()));
    }

    @Test
    void addTeam_should_addTeam() {
        Team team = initializeTeam(managementSystemRepository);

        assertTrue(managementSystemRepository.teamExists(team.getName()));
    }

    @Test
    void addBoard_should_addBoard() {
        Board board = initializeBoard(managementSystemRepository);

        assertTrue(managementSystemRepository.boardInTeamExists(board.getName(), MEMBER_VALID_NAME));
    }

    @Test
    void createBug_should_addBug() {
        Bug bug = initializeBug(managementSystemRepository);

        assertTrue(managementSystemRepository.bugExists(bug.getId()));
    }

    @Test
    void createStory_should_addStory() {
        Story story = initializeStory(managementSystemRepository);

        assertTrue(managementSystemRepository.storyExists(story.getId()));
    }

    @Test
    void createFeedback_should_addFeedback() {
        Feedback feedback = initializeFeedback(managementSystemRepository);

        assertTrue(managementSystemRepository.feedbackExists(feedback.getId()));
    }
    @Test
    void findPersonByName_should_returnPerson_when_exists() {
        Member person = initializePerson(managementSystemRepository);

        assertEquals(person, managementSystemRepository.findPersonByName(person.getName()));
    }

    @Test
    void findPersonByName__should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findPersonByName("Person-name"));
    }

    @Test
    void findMemberByName_should_returnMember_when_exists() {
        Member member = initializeMember(managementSystemRepository);

        assertEquals(member, managementSystemRepository.findMemberByName(member.getName()));
    }

    @Test
    void findChairByModelCode_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findMemberByName("Member-name"));
    }

    @Test
    void findTeamByName_should_returnTeam_when_exists() {
        Team team = initializeTeam(managementSystemRepository);

        assertEquals(team, managementSystemRepository.findTeamByName(TEAM_VALID_NAME));
    }

    @Test
    void findTeamByName_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findTeamByName("Team-name"));
    }

    @Test
    void findBoardInTeamByName_should_returnBoard_when_exists() {

        Board board = initializeBoard(managementSystemRepository);

        assertEquals(board, managementSystemRepository.findBoardByTeamName(board.getName(), TEAM_VALID_NAME));
    }

    @Test
    void findBoardByName_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findBoardByTeamName("Board-name", TEAM_VALID_NAME));
    }

    @Test
    void findBugdById_should_returnBug_when_exists() {
        Bug bug = initializeBug(managementSystemRepository);

        assertEquals(bug, managementSystemRepository.findBugById(bug.getId()));
    }

    @Test
    void findBugdById_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findBugById(1));
    }

    @Test
    void findStoryById_should_returnStory_when_exists() {
        Story story = initializeStory(managementSystemRepository);

        assertEquals(story, managementSystemRepository.findStoryById(1));
    }

    @Test
    void findStoryById_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findStoryById(1));
    }

    @Test
    void findFeedbackById_should_returnFeedback_when_exists() {
        Feedback feedback = initializeFeedback(managementSystemRepository);

        assertEquals(feedback, managementSystemRepository.findFeedbackById(1));
    }

    @Test
    void findFeedbackById_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findFeedbackById(1));
    }

    @Test
    void findTaskById_should_returnTask_when_exists() {
        Task task = initializeBug(managementSystemRepository);

        assertEquals(task, managementSystemRepository.findTaskById(1));
    }

    @Test
    void findTaskById_should_throwException_when_doesntExist() {
        assertThrows(ElementNotFoundException.class,
                () -> managementSystemRepository.findTaskById(1));
    }

    @Test
    void addPersonToTeam_should_createMemberOfTeam_when_exists() {
        Member freePerson = initializePerson(managementSystemRepository);
        Team team = initializeTeam(managementSystemRepository);
        managementSystemRepository.addPersonToTeam(freePerson, team);

        assertEquals(0, managementSystemRepository.getFreePeople().size());
        assertEquals(1, managementSystemRepository.getAllMembers().size());
    }

}
