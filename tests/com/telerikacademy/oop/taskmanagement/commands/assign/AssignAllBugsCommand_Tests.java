package com.telerikacademy.oop.taskmanagement.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.assign.AssignAllBugsCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.Severity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.assign.AssignAllBugsCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_FEATURE;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_PRIORITY;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class AssignAllBugsCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new AssignAllBugsCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_noBugs() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        List<String> arguments = List.of(
                member.getName(),
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_typeIsInvalid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(
                member.getName(),
                "Invalid_BugFeature",
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_bugAlreadyAssigned() {
        // Arrange, Act
        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of(
                MEMBER_VALID_NAME,
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberOfBugDoesNotExist() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(
                MEMBER_VALID_NAME,
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_assignMemberToBug_when_inputOfPriorityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(1, bug.getAssignees().size());
    }

    @Test
    public void execute_should_assignMemberToBug_when_inputOfSeverityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                "Severity",
                Severity.MINOR.toString()));

        // Assert
        Assertions.assertEquals(1, bug.getAssignees().size());
    }

    @Test
    public void execute_should_assignMemberToBug_when_inputOfBugStatusIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                "BugStatus",
                BugStatus.ACTIVE.toString()));

        // Assert
        Assertions.assertEquals(1, bug.getAssignees().size());
    }

    @Test
    public void execute_should_assignBugsToMember_when_inputOfPriorityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                BUG_VALID_FEATURE.toString(),
                BUG_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }

    @Test
    public void execute_should_assignBugToMember_when_inputOfSeverityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                "Severity",
                Severity.MINOR.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }

    @Test
    public void execute_should_assignBugToMember_when_inputOfBugStatusIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeBug(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                "BugStatus",
                BugStatus.ACTIVE.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }
}
