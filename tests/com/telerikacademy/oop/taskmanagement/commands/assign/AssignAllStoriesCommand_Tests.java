package com.telerikacademy.oop.taskmanagement.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.assign.AssignAllStoriesCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.StoryFeature;
import com.telerikacademy.oop.managementsystem.models.enums.StorySize;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.assign.AssignAllBugsCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.STORY_VALID_FEATURE;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.STORY_VALID_PRIORITY;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class AssignAllStoriesCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new AssignAllStoriesCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_noBugs() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        List<String> arguments = List.of(
                member.getName(),
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_typeIsInvalid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        List<String> arguments = List.of(
                member.getName(),
                "Invalid_StoryFeature",
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_bugAlreadyAssigned() {
        // Arrange, Act
        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(
                MEMBER_VALID_NAME,
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberOfStoryDoesNotExist() {
        // Arrange, Act
        initializeStory(managementSystemRepository);
        List<String> arguments = List.of(
                MEMBER_VALID_NAME,
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString());

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_assignMemberToStory_when_inputOfPriorityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(1, story.getAssignees().size());
    }

    @Test
    public void execute_should_assignMemberToStory_when_inputOfSizeIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                StoryFeature.SIZE.toString(),
                StorySize.SMALL.toString()));

        // Assert
        Assertions.assertEquals(1, story.getAssignees().size());
    }

    @Test
    public void execute_should_assignMemberToStory_when_inputOfStoryStatusIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                StoryFeature.STATUS.toString(),
                StoryStatus.NOTDONE.toString()));

        // Assert
        Assertions.assertEquals(1, story.getAssignees().size());
    }

    @Test
    public void execute_should_assignStoriesToMember_when_inputOfPriorityIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                STORY_VALID_FEATURE.toString(),
                STORY_VALID_PRIORITY.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }

    @Test
    public void execute_should_assignStoryToMember_when_inputOfStorySizeIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                StoryFeature.SIZE.toString(),
                StorySize.SMALL.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }

    @Test
    public void execute_should_assignStoryToMember_when_inputOfBugStatusIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        initializeStory(managementSystemRepository);
        command.execute(List.of(
                member.getName(),
                StoryFeature.STATUS.toString(),
                StoryStatus.NOTDONE.toString()));

        // Assert
        Assertions.assertEquals(1, member.getTasks().size());
    }
}
