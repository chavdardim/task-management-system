package com.telerikacademy.oop.taskmanagement.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.assign.UnassignCommand;
import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.assign.UnassignCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializeStory;

public class UnassignCommand_Tests {
    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new UnassignCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_idIsInvalid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        List<String> arguments = List.of(
                member.getName(),
                "Not a number");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberOfBugDoesNotExist() {
        // Arrange, Act
        Bug bug = initializeBug(managementSystemRepository);
        List<String> arguments = List.of(String.valueOf(bug.getId()), "Not a valid member");

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberNotAssignedToBug() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        List<String> arguments = List.of(String.valueOf(bug.getId()), member.getName());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_unassignBugFromMember_when_inputIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        member.addTask(bug);
        command.execute(List.of(String.valueOf(bug.getId()), member.getName()));

        // Assert
        Assertions.assertEquals(0, member.getTasks().size());
    }

    @Test
    public void execute_should_unassignMemberFromBug_when_inputIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        member.addTask(bug);
        List<Member> members = new ArrayList<>();
        members.add(member);
        bug.setAssignees(members);
        command.execute(List.of(String.valueOf(bug.getId()), member.getName()));

        // Assert
        Assertions.assertEquals(0, bug.getAssignees().size());
    }

    @Test
    public void execute_should_throwException_when_memberOfStoryDoesNotExist() {
        // Arrange, Act
        Story story = initializeStory(managementSystemRepository);
        List<String> arguments = List.of(String.valueOf(story.getId()), "Not a valid member");

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_memberNotAssignetToStory() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        List<String> arguments = List.of(String.valueOf(story.getId()), member.getName());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_unassignStoryFromMember_when_inputIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        member.addTask(story);
        command.execute(List.of(String.valueOf(story.getId()), member.getName()));

        // Assert
        Assertions.assertEquals(0, member.getTasks().size());
    }

    @Test
    public void execute_should_unassignMemberFromStory_when_inputIsValid() {
        // Arrange, Act
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        member.addTask(story);
        List<Member> members = new ArrayList<>();
        members.add(member);
        story.setAssignees(members);
        command.execute(List.of(String.valueOf(story.getId()), member.getName()));

        // Assert
        Assertions.assertEquals(0, story.getAssignees().size());
    }
}
