package com.telerikacademy.oop.taskmanagement.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.filter.FilterTaskTypeByStatusCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterTaskTypeByStatusCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Feedback.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class FilterTaskTypeByStatusCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new FilterTaskTypeByStatusCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString(), BUG_VALID_BUGSTATUS.toString());

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_tasktypeIsInvalid() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_PRIORITY.toString(), BUG_VALID_BUGSTATUS.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_taskTypeStatusIsInvalid() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString(), STORY_VALID_STORYSTATUS.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeBug(managementSystemRepository);
        List<String> arguments = List.of( BUG_VALID_TASKTYPE.toString(), BUG_VALID_BUGSTATUS.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeStoryStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeStory(managementSystemRepository);
        List<String> arguments = List.of( STORY_VALID_TASKTYPE.toString(), STORY_VALID_STORYSTATUS.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeFeedbackStatusWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeFeedback(managementSystemRepository);
        List<String> arguments = List.of( FEEDBACK_VALID_TASKTYPE.toString(), FEEDBACK_VALID_STATUS.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}