package com.telerikacademy.oop.taskmanagement.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.sorting.SortTaskTypeByTitleCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;

import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.sorting.SortTaskTypeByTitleCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Feedback.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class SortTaskTypeByTitle_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new SortTaskTypeByTitleCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act

        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeStoryWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeStory(managementSystemRepository);
        List<String> arguments = List.of(STORY_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeFeedbackWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeFeedback(managementSystemRepository);
        List<String> arguments = List.of(FEEDBACK_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}
