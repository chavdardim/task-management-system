package com.telerikacademy.oop.taskmanagement.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.sorting.SortTaskTypeBySeverityCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_TASKTYPE;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class SortTaskTypeBySeverity_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new SortTaskTypeBySeverityCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_thereIsNoTasks() {
        // Arrange, Act
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}
