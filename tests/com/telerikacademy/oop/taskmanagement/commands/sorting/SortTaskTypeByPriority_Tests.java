package com.telerikacademy.oop.taskmanagement.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.sorting.SortTaskTypeByPriorityCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.filter.FilterAllTasksByAssigneeCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.BUG_VALID_TASKTYPE;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.STORY_VALID_TASKTYPE;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Feedback.FEEDBACK_VALID_TASKTYPE;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class SortTaskTypeByPriority_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new SortTaskTypeByPriorityCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_thereIsTasks() {
        // Arrange, Act

        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_taskTypeIsInvalid() {
        // Arrange, Act
        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of(FEEDBACK_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertThrows(InvalidUserInputException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeBugWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeAssignedBug(managementSystemRepository);
        List<String> arguments = List.of(BUG_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeStoryWithoutException_when_inputIsValid() {
        // Arrange, Act

        initializeAssignedStory(managementSystemRepository);
        List<String> arguments = List.of(STORY_VALID_TASKTYPE.toString());

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }

}
