package com.telerikacademy.oop.taskmanagement.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.show.ShowPeopleCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializePerson;

public class ShowPeopleCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ShowPeopleCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_thereIsNoPeople() {
        // Arrange, Act

        List<String> arguments = List.of();

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializePerson(managementSystemRepository);
        command.execute(List.of());

        // Assert
        Assertions.assertEquals(1, managementSystemRepository.getFreePeople().size());
    }
}
