package com.telerikacademy.oop.taskmanagement.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.show.ShowCommentsCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.show.ShowCommentsCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class ShowCommentsCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ShowCommentsCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS - 1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_idIsInvalid() {
        // Arrange, Act
        List<String> arguments = List.of("Not a number");

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_Task_NotExist() {
        // Arrange, Act
        Task bug = initializeBug(managementSystemRepository);
        List<String> arguments = List.of(String.valueOf(bug.getId() + 1));

        // Assert
        Assertions.assertThrows(ElementNotFoundException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_show_comments_when_InputIsValid() {
        // Arrange, Act
        Task bug = initializeBug(managementSystemRepository);
        bug.addComment("Comment_test");

        command.execute(List.of(String.valueOf(bug.getId())));

        // Assert
        Assertions.assertEquals("Comment_test", managementSystemRepository
                .getAllBugs().get(0).getComments().get(0));

    }

}
