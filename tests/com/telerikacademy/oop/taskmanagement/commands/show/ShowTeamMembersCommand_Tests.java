package com.telerikacademy.oop.taskmanagement.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.commands.show.ShowTeamMembersCommand;
import com.telerikacademy.oop.managementsystem.core.ManagementSystemRepositoryImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.commands.show.ShowTeamMembersCommand.EXPECTED_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Team.TEAM_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.*;

public class ShowTeamMembersCommand_Tests {

    private Command command;
    private ManagementSystemRepository managementSystemRepository;

    @BeforeEach
    public void before() {
        managementSystemRepository = new ManagementSystemRepositoryImpl();
        command = new ShowTeamMembersCommand(managementSystemRepository);
    }

    @Test
    public void execute_should_throwException_when_argumentsCountDifferentThanExpected() {
        // Arrange
        List<String> arguments = initializeListWithSize(EXPECTED_NUMBER_OF_ARGUMENTS-1);

        // Act, Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange, Act

        List<String> arguments = List.of(TEAM_VALID_NAME);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_throwException_when_teamDoesNotHaveMembers() {
        // Arrange, Act
        initializeTeam(managementSystemRepository);
        List<String> arguments = List.of(TEAM_VALID_NAME);

        // Assert
        Assertions.assertThrows(IllegalArgumentException.class, () -> command.execute(arguments));
    }

    @Test
    public void execute_should_executeWithoutException_when_inputIsValid() {
        // Arrange, Act
        initializeMember(managementSystemRepository);
        List<String> arguments = List.of(TEAM_VALID_NAME);

        // Assert
        Assertions.assertDoesNotThrow(() -> command.execute(arguments));
    }
}
