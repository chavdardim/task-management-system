package com.telerikacademy.oop.taskmanagement.utils;

import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.*;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.oop.taskmanagement.utils.TestData.Member.MEMBER_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Board.BOARD_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Team.TEAM_VALID_NAME;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Bug.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Story.*;
import static com.telerikacademy.oop.taskmanagement.utils.TestData.Feedback.*;
import static java.util.Arrays.asList;

public class TestUtilities {

    public static Member initializePerson(ManagementSystemRepository managementSystemRepository) {
        managementSystemRepository.addPerson(MEMBER_VALID_NAME);
        return managementSystemRepository.findPersonByName(MEMBER_VALID_NAME);
    }

    public static Board initializeBoard(ManagementSystemRepository managementSystemRepository) {
        Board board = createBoard();
        Team team = initializeTeam(managementSystemRepository);
        team.addBoard(board);
        return board;
    }

    public static Team initializeTeam(ManagementSystemRepository managementSystemRepository) {
        managementSystemRepository.addTeam(TEAM_VALID_NAME);
        return managementSystemRepository.findTeamByName(TEAM_VALID_NAME);
    }

    public static Member initializeMember(ManagementSystemRepository managementSystemRepository) {
        Team team = initializeTeam(managementSystemRepository);
        Member person = initializePerson(managementSystemRepository);
        team.addMember(person);
        return person;
    }
    public static Bug initializeBug(ManagementSystemRepository managementSystemRepository) {
        return managementSystemRepository.createBug(
                BUG_VALID_TITLE,
                BUG_VALID_DESCRIPTION
        );
    }

    public static void initializeAssignedBug(ManagementSystemRepository managementSystemRepository) {
        Member member = initializeMember(managementSystemRepository);
        Bug bug = initializeBug(managementSystemRepository);
        List<Member> members = new ArrayList<>();
        members.add(member);
        bug.setAssignees(members);
        member.addTask(bug);
    }

    public static Story initializeStory(ManagementSystemRepository managementSystemRepository) {
        return managementSystemRepository.createStory(
                STORY_VALID_TITLE,
                STORY_VALID_DESCRIPTION
        );
    }

    public static void initializeAssignedStory(ManagementSystemRepository managementSystemRepository) {
        Member member = initializeMember(managementSystemRepository);
        Story story = initializeStory(managementSystemRepository);
        List<Member> members = new ArrayList<>();
        members.add(member);
        story.setAssignees(members);
        member.addTask(story);
    }

    public static Feedback initializeFeedback(ManagementSystemRepository managementSystemRepository) {
        return managementSystemRepository.createFeedback(
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION
        );
    }

    public static Member createMember() {
        return new MemberImpl(MEMBER_VALID_NAME);
    }

    public static Board createBoard() {
        return new BoardImpl(BOARD_VALID_NAME);
    }

    public static Team createTeam() {
        return new TeamImpl(TEAM_VALID_NAME);
    }

    public static Bug createBug() {
        return new BugImpl(
                1,
                BUG_VALID_TITLE,
                BUG_VALID_DESCRIPTION);
    }

    public static Story createStory() {
        return new StoryImpl(
                1,
                STORY_VALID_TITLE,
                STORY_VALID_DESCRIPTION
        );
    }

    public static Feedback createFeedback() {
        return new FeedbackImpl(
                1,
                FEEDBACK_VALID_TITLE,
                FEEDBACK_VALID_DESCRIPTION
        );
    }


    /**
     * Returns a new String with size equal to wantedSize.
     * Useful when you do not care what the value of the String is,
     * for example when testing if a String is of a certain size.
     *
     * @param wantedSize the size of the String to be returned.
     * @return a new String with size equal to wantedSize
     */
    public static String initializeStringWithSize(int wantedSize) {
        return "x".repeat(wantedSize);
    }

    /**
     * Returns a new List with size equal to wantedSize.
     * Useful when you do not care what the contents of the List are,
     * for example when testing if a list of a command throws exception
     * when it's parameters list's size is less/more than expected.
     *
     * @param wantedSize the size of the List to be returned.
     * @return a new List with size equal to wantedSize
     */
    public static List<String> initializeListWithSize(int wantedSize) {
        return asList(new String[wantedSize]);
    }

}
