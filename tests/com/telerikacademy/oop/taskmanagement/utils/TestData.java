package com.telerikacademy.oop.taskmanagement.utils;

import com.telerikacademy.oop.managementsystem.models.BoardImpl;
import com.telerikacademy.oop.managementsystem.models.BugImpl;
import com.telerikacademy.oop.managementsystem.models.TaskBaseImpl;
import com.telerikacademy.oop.managementsystem.models.MemberImpl;
import com.telerikacademy.oop.managementsystem.models.TeamImpl;
import com.telerikacademy.oop.managementsystem.models.enums.*;

import static com.telerikacademy.oop.taskmanagement.utils.TestUtilities.initializeStringWithSize;

public class TestData {

    public static class Member {
        public static String MEMBER_VALID_NAME = initializeStringWithSize(MemberImpl.NAME_MIN_LENGTH + 1);
    }
    public static class Board {
        public static String BOARD_VALID_NAME = initializeStringWithSize(BoardImpl.NAME_MIN_LENGTH + 1);
    }

    public static class Team {
        public static String TEAM_VALID_NAME = initializeStringWithSize(TeamImpl.NAME_MIN_LENGTH + 1);
    }

    public static class Bug {
        public static int BUG_VALID_ID = 1;
        public static String BUG_VALID_TITLE = initializeStringWithSize(BugImpl.TITLE_MIN_LENGTH + 1);
        public static String BUG_VALID_DESCRIPTION = initializeStringWithSize(BugImpl.DESCRIPTION_MIN_LENGTH + 1);
        public static TaskType BUG_VALID_TASKTYPE = TaskType.BUG;
        public static BugFeature BUG_VALID_FEATURE = BugFeature.PRIORITY;
        public static BugStatus BUG_VALID_BUGSTATUS = BugStatus.ACTIVE;
        public static Priority BUG_VALID_PRIORITY = Priority.LOW;
        public static String BUG_VALID_REPRODUCING_STEPS = initializeStringWithSize(BugImpl.REPRODUCING_STEPS_MIN_LENGTH+1);
        public static String BUG_VALID_COMMENT = initializeStringWithSize(TaskBaseImpl.COMMENT_MIN_LENGTH+1);
    }

    public static class Story {
        public static int STORY_VALID_ID = 1;
        public static String STORY_VALID_TITLE = initializeStringWithSize(BugImpl.TITLE_MIN_LENGTH + 1);
        public static String STORY_VALID_DESCRIPTION = initializeStringWithSize(BugImpl.DESCRIPTION_MIN_LENGTH + 1);
        public static TaskType STORY_VALID_TASKTYPE = TaskType.STORY;
        public static StoryFeature STORY_VALID_FEATURE = StoryFeature.PRIORITY;
        public static StoryStatus STORY_VALID_STORYSTATUS = StoryStatus.NOTDONE;
        public static Priority STORY_VALID_PRIORITY = Priority.LOW;
    }

    public static class Feedback {
        public static int FEEDBACK_VALID_ID = 1;
        public static String FEEDBACK_VALID_TITLE = initializeStringWithSize(BugImpl.TITLE_MIN_LENGTH + 1);
        public static String FEEDBACK_VALID_DESCRIPTION = initializeStringWithSize(BugImpl.DESCRIPTION_MIN_LENGTH + 1);
        public static TaskType FEEDBACK_VALID_TASKTYPE = TaskType.FEEDBACK;
        public static FeedbackFeature FEEDBACK_VALID_FEATURE = FeedbackFeature.RATING;
        public static FeedbackStatus FEEDBACK_VALID_STATUS = FeedbackStatus.NEW;
        public static int FEEDBACK_VALID_RATING = 1;
    }
}
