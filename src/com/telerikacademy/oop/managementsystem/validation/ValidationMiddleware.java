package com.telerikacademy.oop.managementsystem.validation;

import an.awesome.pipelinr.Command;

import java.util.List;

public class ValidationMiddleware implements Command.Middleware {
    private final List<CommandValidator> validators;

    public ValidationMiddleware(List<CommandValidator> validators) {
        this.validators = validators;
    }

    @Override
    public <R, C extends Command<R>> R invoke(C command, Next<R> next) {
        validators.stream()
                .filter(v -> v.matches(command))
                .findFirst()
                .ifPresent(v -> v.validate(command));
        return next.invoke();
    }
}
