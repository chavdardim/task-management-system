package com.telerikacademy.oop.managementsystem.validation.validators;

import com.telerikacademy.oop.managementsystem.commands.creation.CreateBoard;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.BoardDto;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;
import com.telerikacademy.oop.managementsystem.validation.CommandValidator;

import java.util.List;

import static java.lang.String.format;

public enum CreateBoardValidator implements CommandValidator<CreateBoard.CommandImpl, CommandResult> {
    INSTANCE;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String BOARD_EXISTS_ERROR_MESSAGE = "Board with name %s already exists.";

    @Override
    public void validate(CreateBoard.CommandImpl request) {
        ValidationHelpers.validateArgumentsCount(request.parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String boardName = request.parameters.get(0);
        String teamName = request.parameters.get(1);
        request.boardDto = new BoardDto(boardName, teamName);
        if (request.managementSystemRepository.boardInTeamExists(boardName, teamName)) {
            throw new InvalidUserInputException(format(BOARD_EXISTS_ERROR_MESSAGE, boardName));
        }
    }
}
