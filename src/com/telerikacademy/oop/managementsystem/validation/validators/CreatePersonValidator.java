package com.telerikacademy.oop.managementsystem.validation.validators;

import com.telerikacademy.oop.managementsystem.commands.creation.CreatePerson;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.PersonDto;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;
import com.telerikacademy.oop.managementsystem.validation.CommandValidator;

import static java.lang.String.format;

public enum CreatePersonValidator implements CommandValidator<CreatePerson.CommandImpl, CommandResult> {
    INSTANCE;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    private static final String PERSON_EXISTS_ERROR_MESSAGE = "Person with name %s already exists.";

    @Override
    public void validate(CreatePerson.CommandImpl request) {
        ValidationHelpers.validateArgumentsCount(request.parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String personName = request.parameters.get(0);
        request.personDto = new PersonDto(personName);
        ManagementSystemRepository repository = request.managementSystemRepository;
        if (repository.personExists(personName) || repository.memberExists(personName)) {
            throw new InvalidUserInputException(format(PERSON_EXISTS_ERROR_MESSAGE, personName));
        }
    }
}
