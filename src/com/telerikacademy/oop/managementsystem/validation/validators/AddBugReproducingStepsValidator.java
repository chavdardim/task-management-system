package com.telerikacademy.oop.managementsystem.validation.validators;

import com.telerikacademy.oop.managementsystem.commands.creation.AddBugReproducingSteps;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.BugReproducingStepsDto;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;
import com.telerikacademy.oop.managementsystem.validation.CommandValidator;

public enum AddBugReproducingStepsValidator implements CommandValidator<AddBugReproducingSteps.CommandImpl, CommandResult> {
    INSTANCE;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_TASK_ID = "Task ID should be a valid number";

    @Override
    public void validate(AddBugReproducingSteps.CommandImpl request) {
        ValidationHelpers.validateArgumentsCount(request.parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int bugId = ParsingHelpers.tryParseInt(request.parameters.get(0), INVALID_TASK_ID);
        String reproducingStepsInput = request.parameters.get(1);
        request.bugReproducingStepsDto = new BugReproducingStepsDto(bugId, reproducingStepsInput);
    }
}
