package com.telerikacademy.oop.managementsystem.validation.validators;

import com.telerikacademy.oop.managementsystem.commands.creation.AddMember;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.MemberDto;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;
import com.telerikacademy.oop.managementsystem.validation.CommandValidator;

import static java.lang.String.format;

public enum AddMemberValidator implements CommandValidator<AddMember.CommandImpl, CommandResult> {
    INSTANCE;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String MEMBER_EXIST = "Member with name %s already exist.";


    @Override
    public void validate(AddMember.CommandImpl request) {
        ValidationHelpers.validateArgumentsCount(request.parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        String personName = request.parameters.get(0);
        String teamName = request.parameters.get(1);
        request.memberDto = new MemberDto(personName, teamName);
        if (request.managementSystemRepository.memberExists(personName)) {
            throw new InvalidUserInputException(format(MEMBER_EXIST, personName));
        }

    }
}
