package com.telerikacademy.oop.managementsystem.validation.validators;

import com.telerikacademy.oop.managementsystem.commands.creation.AddComment;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.CommentDto;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;
import com.telerikacademy.oop.managementsystem.validation.CommandValidator;

public enum AddCommentValidator implements CommandValidator<AddComment.CommandImpl, CommandResult> {

    INSTANCE;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    private static final String INVALID_TASK_ID = "Task ID %s should be a valid number";


    @Override
    public void validate(AddComment.CommandImpl request) {
        ValidationHelpers.validateArgumentsCount(request.parameters, EXPECTED_NUMBER_OF_ARGUMENTS);
        int taskId = ParsingHelpers.tryParseInt(
                request.parameters.get(0),
                String.format(INVALID_TASK_ID, request.parameters.get(1)));
        String comment = request.parameters.get(1);
        request.commentDto = new CommentDto(taskId, comment);
    }
}
