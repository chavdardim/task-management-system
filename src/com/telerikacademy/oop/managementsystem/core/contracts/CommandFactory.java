package com.telerikacademy.oop.managementsystem.core.contracts;


import an.awesome.pipelinr.Command;
import com.telerikacademy.oop.managementsystem.models.CommandResult;

import java.util.List;

public interface CommandFactory {

    Command<CommandResult> createCommandFromCommandName(String commandTypeAsString,
                                                        List<String> parameters,
                                                        ManagementSystemRepository managementSystemRepository);

}
