package com.telerikacademy.oop.managementsystem.core.contracts;

public interface ManagementSystemEngine {

    void start();

}
