package com.telerikacademy.oop.managementsystem.core;

import an.awesome.pipelinr.Notification;
import an.awesome.pipelinr.Pipeline;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.*;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Identifiable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Nameable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ManagementSystemRepositoryImpl implements ManagementSystemRepository {

    private int nextId;
    private final List<Team> teams;
    private final List<Member> freePersons;
    private final List<Story> stories;
    private final List<Feedback> feedbacks;
    private final List<Member> allMembers;
    private final List<Notification> notifications;

    public ManagementSystemRepositoryImpl() {
        nextId = 0;
        teams = new ArrayList<>();
        freePersons = new ArrayList<>();
        stories = new ArrayList<>();
        feedbacks = new ArrayList<>();
        allMembers = new ArrayList<>();
        notifications = new ArrayList<>();
    }

    @Override
    public List<Member> getFreePeople() {
        return new ArrayList<>(freePersons);
    }

    @Override
    public List<Member> getAllMembers() {
        for (Team team : teams) {
            allMembers.addAll(team.getMemberList());
        }
        return new ArrayList<>(allMembers);
    }

    @Override
    public List<Team> getTeams() {
        return new ArrayList<>(teams);
    }

    @Override
    public List<Task> getAllTasks() {
        return teams.stream()
                .flatMap(team -> team.getBoards().stream())
                .flatMap(board -> board.getTasks().stream())
                .collect(Collectors.toList());
    }

    public List<Bug> getAllBugs() {
        return getAllTasks().stream()
                .filter(task -> task.getClass() == BugImpl.class)
                .map(task -> (Bug) task)
                .collect(Collectors.toList());
    }

    public List<Story> getAllStories() {
        return new ArrayList<>(stories);
    }

    public List<Feedback> getAllFeedbacks() {
        return new ArrayList<>(feedbacks);
    }

    public List<Notification> getNotifications(){
        return notifications;
    }

    @Override
    public void addPerson(String personName) {
        Member person = new MemberImpl(personName);
        freePersons.add(person);
    }

    @Override
    public void addPersonToTeam(Member freePerson, Team team) {
        team.addMember(freePerson);
        freePersons.remove(freePerson);
    }

    @Override
    public void addTeam(String teamName) {
        Team team = new TeamImpl(teamName);
        teams.add(team);
    }

    @Override
    public void addNotification(Notification notification) {
        notifications.add(notification);
    }

    @Override
    public void dispatchNotifications(Pipeline pipeline) {
        notifications.forEach(pipeline::send);
        notifications.clear();
    }

    @Override
    public Board createBoard(String title) {
        return new BoardImpl(title);
    }

    @Override
    public Story createStory(String title, String description) {
        Story story = new StoryImpl(++nextId, title, description);
        stories.add(story);
        return story;
    }

    @Override
    public Feedback createFeedback(String title, String description) {
        Feedback feedback = new FeedbackImpl(++nextId, title, description);
        feedbacks.add(feedback);
        return feedback;
    }

    @Override
    public boolean personExists(String personName) {
        return nameExists(getFreePeople(), personName);
    }

    @Override
    public boolean memberExists(String personName) {
        return nameExists(getAllMembers(), personName);
    }

    @Override
    public boolean teamExists(String teamName) {
        return nameExists(teams, teamName);
    }

    @Override
    public boolean boardExists(String boardName) {
        List<Board> boards = teams.stream()
                .flatMap(team -> team.getBoards().stream())
                .collect(Collectors.toList());
        return nameExists(boards, boardName);
    }

    @Override
    public boolean boardInTeamExists(String boardName, String teamName) {
        return nameExists(findTeamByName(teamName).getBoards(), boardName);
    }

    private <T extends Nameable> boolean nameExists(List<T> items, String name) {
        return items
                .stream()
                .anyMatch(item -> item.getName().equals(name));
    }

    @Override
    public boolean bugExists(int id) {
        return idExists(id, BugImpl.class);
    }

    @Override
    public boolean storyExists(int id) {
        return idExists(stories, id);
    }

    @Override
    public boolean feedbackExists(int id) {
        return idExists(feedbacks, id);
    }

    private <T extends Identifiable> boolean idExists(List<T> items, int id) {
        return items
                .stream()
                .anyMatch(item -> item.getId() == id);
    }

    private <T extends Identifiable> boolean idExists(int id, Class<T> clazz) {
        return getAllTasks()
                .stream()
                .anyMatch(item -> item.getId() == id && item.getClass() == clazz);
    }

    @Override
    public Member findMemberByName(String personName) {
        return findItemByName(getAllMembers(), personName, "Member");
    }

    @Override
    public Member findPersonByName(String personName) {
        return findItemByName(getFreePeople(), personName, "Person");
    }


    @Override
    public Team findTeamByName(String name) {
        return findItemByName(teams, name, "Team");
    }

    @Override
    public Board findBoardByName(String name) {
        List<Board> boards = teams.stream()
                .flatMap(team -> team.getBoards().stream())
                .collect(Collectors.toList());
        return findItemByName(boards, name, "Board");
    }

    @Override
    public Board findBoardByTeamName(String boardName, String teamName) {
        List<Board> boards = findTeamByName(teamName).getBoards();
        return findItemByName(boards, boardName, "Board");
    }

    private <T extends Nameable> T findItemByName(List<T> items, String name, String type) {
        return items
                .stream()
                .filter(item -> item.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format("No %s with name %s", type, name)));
    }

    @Override
    public Task findTaskById(int id) {
        return findItemById(getAllTasks(), id, "task");
    }

    @Override
    public Bug findBugById(int id) {
        return findItemById(id, Bug.class);
    }

    @Override
    public Story findStoryById(int id) {
        return findItemById(stories, id, "story");
    }

    @Override
    public Feedback findFeedbackById(int id) {
        return findItemById(feedbacks, id, "feedback");
    }

    private <T extends Identifiable> T findItemById(List<T> items, int id, String type) {
        return items
                .stream()
                .filter(item -> item.getId() == id)
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format("No %s with ID %d", type, id)));
    }

    @Override
    public <T extends Identifiable> T findItemById(int id, Class<T> clazz) {
        return getAllTasks()
                .stream()
                .filter(item -> item.getId() == id && clazz.isInstance(item))
                .map(clazz::cast)
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(
                        String.format("No %s with ID %d", clazz.getSimpleName().toLowerCase(), id)));
    }
}
