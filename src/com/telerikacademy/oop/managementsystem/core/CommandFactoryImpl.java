package com.telerikacademy.oop.managementsystem.core;

import an.awesome.pipelinr.Command;
import com.telerikacademy.oop.managementsystem.commands.assign.AssignAllBugsCommand;
import com.telerikacademy.oop.managementsystem.commands.assign.AssignAllStoriesCommand;
import com.telerikacademy.oop.managementsystem.commands.assign.AssignCommand;
import com.telerikacademy.oop.managementsystem.commands.assign.UnassignCommand;
import com.telerikacademy.oop.managementsystem.commands.change.ChangeBugCommand;
import com.telerikacademy.oop.managementsystem.commands.change.ChangeFeedbackCommand;
import com.telerikacademy.oop.managementsystem.commands.change.ChangeStoryCommand;
import com.telerikacademy.oop.managementsystem.commands.creation.*;
import com.telerikacademy.oop.managementsystem.commands.enums.CommandType;
import com.telerikacademy.oop.managementsystem.commands.filter.*;
import com.telerikacademy.oop.managementsystem.commands.show.ShowActivityCommand;
import com.telerikacademy.oop.managementsystem.commands.show.ShowCommentsCommand;
import com.telerikacademy.oop.managementsystem.commands.show.ShowPeopleCommand;
import com.telerikacademy.oop.managementsystem.commands.show.ShowTeamMembersCommand;
import com.telerikacademy.oop.managementsystem.commands.sorting.*;
import com.telerikacademy.oop.managementsystem.core.contracts.CommandFactory;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;

import java.util.List;

public class CommandFactoryImpl implements CommandFactory {

    @Override
    public Command<CommandResult> createCommandFromCommandName(String commandTypeAsString,
                                                               List<String> parameters,
                                                               ManagementSystemRepository managementSystemRepository) {
        CommandType commandType = ParsingHelpers.tryParseEnum(commandTypeAsString, CommandType.class);
        return switch (commandType) {
            //CREATION:
            case ADDCOMMENT -> new AddComment.CommandImpl(managementSystemRepository, parameters);
            case ADDMEMBER -> new AddMember.CommandImpl(managementSystemRepository, parameters);
            case ADDREPRODUCINGSTEPS -> new AddBugReproducingSteps.CommandImpl(managementSystemRepository, parameters);
            case CREATEBOARD -> new CreateBoard.CommandImpl(managementSystemRepository, parameters);
            case CREATEPERSON -> new CreatePerson.CommandImpl(managementSystemRepository, parameters);
//            case CREATETASK -> new CreateTaskCommand(managementSystemRepository);
//            case CREATETEAM -> new CreateTeamCommand(managementSystemRepository);
//
//            //ASSIGNING:
//            case ASSIGNALLBUGS -> new AssignAllBugsCommand(managementSystemRepository);
//            case ASSIGNALLSTORIES -> new AssignAllStoriesCommand(managementSystemRepository);
//            case ASSIGN -> new AssignCommand(managementSystemRepository);
//            case UNASSIGN -> new UnassignCommand(managementSystemRepository);
//
//            //CHANGE:
//            case CHANGEBUG -> new ChangeBugCommand(managementSystemRepository);
//            case CHANGEFEEDBACK -> new ChangeFeedbackCommand(managementSystemRepository);
//            case CHANGESTORY -> new ChangeStoryCommand(managementSystemRepository);
//
//            //FILTERING:
//            case FILTERALLTASKSBYASSIGNEE -> new FilterAllTasksByAssigneeCommand(managementSystemRepository);
//            case FILTERALLTASKSBYSTATUSANDASSIGNEE ->
//                    new FilterAllTasksByStatusAndAssigneeCommand(managementSystemRepository);
//            case FILTERALLTASKSBYSTATUS -> new FilterAllTasksByStatusCommand(managementSystemRepository);
//            case FILTERALLTASKSBYTITLEKEYWORD -> new FilterAllTasksByTitleKeywordCommand(managementSystemRepository);
//            case FILTERALLTASKSWITHOUTASSIGNEE -> new FilterAllTasksWithoutAssigneeCommand(managementSystemRepository);
//            case FILTERTASKTYPEBYASSIGNEE -> new FilterTaskTypeByAssigneeCommand(managementSystemRepository);
//            case FILTERTASKTYPEBYSTATUS -> new FilterTaskTypeByStatusCommand(managementSystemRepository);
//
//            //SHOW:
//            case SHOWPEOPLE -> new ShowPeopleCommand(managementSystemRepository);
//            case SHOWTEAMMEMBERS -> new ShowTeamMembersCommand(managementSystemRepository);
//            case SHOWCOMMENTS -> new ShowCommentsCommand(managementSystemRepository);
//            case SHOWACTIVITY -> new ShowActivityCommand(managementSystemRepository);
//
//            //SORTING:
//            case SORTALLASSIGNEDTASKSBYTITLE -> new SortAllAssignedTasksByTitleCommand(managementSystemRepository);
//            case SORTALLTASKSBYTITLE -> new SortAllTasksByTitleCommand(managementSystemRepository);
//            case SORTTASKTYPEBYPRIORITY -> new SortTaskTypeByPriorityCommand(managementSystemRepository);
//            case SORTTASKTYPEBYRATING -> new SortTaskTypeByRatingCommand(managementSystemRepository);
//            case SORTTASKTYPEBYSEVERITY -> new SortTaskTypeBySeverityCommand(managementSystemRepository);
//            case SORTTASKTYPEBYSIZE -> new SortTaskTypeBySizeCommand(managementSystemRepository);
//            case SORTTASKTYPEBYTITLE -> new SortTaskTypeByTitleCommand(managementSystemRepository);
            default -> throw new IllegalArgumentException();
        };
    }

}
