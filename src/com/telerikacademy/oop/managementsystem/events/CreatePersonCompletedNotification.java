package com.telerikacademy.oop.managementsystem.events;

import an.awesome.pipelinr.Notification;
import com.telerikacademy.oop.managementsystem.models.CommandResult;

public class CreatePersonCompletedNotification implements CreatePersonNotification {
    public final CommandResult commandResult;

    public CreatePersonCompletedNotification(CommandResult commandResult) {
        this.commandResult = commandResult;
    }
}

