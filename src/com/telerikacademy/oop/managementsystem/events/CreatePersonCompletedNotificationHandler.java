package com.telerikacademy.oop.managementsystem.events;

import an.awesome.pipelinr.Notification;

public class CreatePersonCompletedNotificationHandler implements Notification.Handler<CreatePersonCompletedNotification> {

    @Override
    public void handle(CreatePersonCompletedNotification notification) {
        System.out.println("Notification: " + notification.commandResult.message());
    }
}
