package com.telerikacademy.oop.managementsystem.models.dtos;

public record CommentDto(int taskId, String comment) {
}
