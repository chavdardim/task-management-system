package com.telerikacademy.oop.managementsystem.models.dtos;

public record BugReproducingStepsDto(int bugId, String reproducingSteps) {
}
