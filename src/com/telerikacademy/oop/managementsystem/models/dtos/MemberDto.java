package com.telerikacademy.oop.managementsystem.models.dtos;

public record MemberDto(String personName, String teamName) {
}
