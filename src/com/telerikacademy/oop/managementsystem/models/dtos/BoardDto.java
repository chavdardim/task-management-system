package com.telerikacademy.oop.managementsystem.models.dtos;

public record BoardDto(String boardName, String teamName) {
}
