package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Commentable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public abstract class TaskBaseImpl implements Task, Commentable {

    public static final int TITLE_MIN_LENGTH = 10;
    public static final int TITLE_MAX_LENGTH = 50;
    public static final String INVALID_TITLE_LENGTH =
            String.format("Please enter title between %d and %d symbols long",
                    TITLE_MIN_LENGTH,
                    TITLE_MAX_LENGTH);

    public static final int DESCRIPTION_MIN_LENGTH = 10;
    public static final int DESCRIPTION_MAX_LENGTH = 500;
    public static final String INVALID_DESCRIPTION_LENGTH =
            String.format("Please enter description between %d and %d symbols long",
                    DESCRIPTION_MIN_LENGTH,
                    DESCRIPTION_MAX_LENGTH);
    public static final int COMMENT_MIN_LENGTH = 10;
    public static final int COMMENT_MAX_LENGTH = 150;
    public static final String COMMENT_INVALID_INPUT =
            String.format("Please enter comment between %d and %d symbols long",
                    COMMENT_MIN_LENGTH,
                    COMMENT_MAX_LENGTH);


    private int id;
    private String title;
    private String description;
    private final List<String> comments;
    private final List<Activity> historyChanges;

    public TaskBaseImpl(int id, String title, String description) {
        setId(id);
        setTitle(title);
        setDescription(description);
        comments = new ArrayList<>();
        historyChanges = new ArrayList<>();
    }

    private void setId(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return id;
    }

    private void setTitle(String title) {
        ValidationHelpers.validateStringLength(
                title,
                TITLE_MIN_LENGTH,
                TITLE_MAX_LENGTH,
                INVALID_TITLE_LENGTH);
        this.title = title;
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void setDescription(String description) {
        ValidationHelpers.validateStringLength(
                description,
                DESCRIPTION_MIN_LENGTH,
                DESCRIPTION_MAX_LENGTH,
                INVALID_DESCRIPTION_LENGTH);
        this.description = description;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public abstract String getStatus();

    @Override
    public abstract TaskType getTaskType();

    public void addComment(String comment) {
        ValidationHelpers.validateStringLength(
                comment,
                COMMENT_MIN_LENGTH,
                COMMENT_MAX_LENGTH,
                COMMENT_INVALID_INPUT);
        comments.add(comment);
    }

    @Override
    public List<String> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void logActivity(Activity event) {
        historyChanges.add(event);
    }

    @Override
    public List<Activity> getActivity() {
        return new ArrayList<>(historyChanges);
    }

    @Override
    public String getAsString() {
        StringBuilder result = new StringBuilder();
        String commentCount = "--Comments--";
        result.append(String.format("%s%n" +
                "%s%n" +
                "%s%n",
                getImportantInfo(),
                commentCount,
                getComments().isEmpty() ? "no comments" : comments));
        result.append("--History Log--\n");
        for (Activity historyChange : historyChanges) {
            result.append(historyChange.viewInfo()).append("\n");
        }
        return result.toString();
    }

    @Override
    public String getImportantInfo() {
        return String.format(" %s ID: %d%n" +
                        "Title: %s%n" +
                        "Description: %s%n" +
                        "%s",
                getClass().getSimpleName().replace("Impl",""),
                id,
                title,
                description,
                getAdditionalInfo());
    }

    @Override
    public abstract String getAdditionalInfo();

}
