package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class TeamImpl implements Team {
    public static int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 15;
    public static final String INVALID_NAME_LENGTH =
            String.format(
                    "Invalid input. Name should be between %d and %d symbols long.",
                    NAME_MIN_LENGTH,
                    NAME_MAX_LENGTH);

    private String teamName;
    private final List<Member> members;
    private final List<Board> boards;
    private final List<Activity> history;


    public TeamImpl(String teamName) {
        setName(teamName);
        members = new ArrayList<>();
        boards = new ArrayList<>();
        history = new ArrayList<>();
        history.add(new ActivityImpl(String.format("Team %s was created.", teamName)));
    }

    @Override
    public String getName() {
        return teamName;
    }

    private void setName(String teamName){
        ValidationHelpers.validateStringLength(
                teamName,
                NAME_MIN_LENGTH,
                NAME_MAX_LENGTH,
                INVALID_NAME_LENGTH);
        this.teamName = teamName;
    }

    @Override
    public List<Board> getBoards() {
        return new ArrayList<>(boards);
    }

    @Override
    public void addBoard(Board board) {
        boards.add(board);
        history.add(new ActivityImpl(String.format(
                "Board %s added to %s", board.getName(), teamName)));
    }

    @Override
    public List<Member> getMemberList() {
        return new ArrayList<>(members);
    }

    @Override
    public void addMember(Member member) {
        members.add(member);
        history.add(new ActivityImpl(String.format(
                "Member %s added to %s", member.getName(), teamName)));
    }

    @Override
    public List<Activity> getActivity() {
        return new ArrayList<>(history);
    }

    @Override
    public void logActivity(Activity event) {
        history.add(event);
    }
}
