package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.List;

public class BoardImpl implements Board {

    public static final String TASK_REMOVED_MSG = "%s with ID %d removed from %s";
    public static final String TASK_ADDED_MSG = "%s with ID %d added to %s";
    public static int NAME_MIN_LENGTH = 5;
    public static final int NAME_MAX_LENGTH = 10;
    public static final String INVALID_NAME_LENGTH =
            String.format(
                    "Invalid input. Name should be between %d and %d symbols long.",
                    NAME_MIN_LENGTH,
                    NAME_MAX_LENGTH);

    private String name;
    private final List<Task> tasks;
    private final List<Activity> history;


    public BoardImpl(String name) {
        setName(name);
        this.tasks = new ArrayList<>();
        this.history = new ArrayList<>();
        history.add(new ActivityImpl(String.format("Board %s was created.", name)));
    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        ValidationHelpers.validateStringLength(
                name,
                NAME_MIN_LENGTH,
                NAME_MAX_LENGTH,
                INVALID_NAME_LENGTH);
        this.name = name;
    }

    @Override
    public List<Task> getTasks() {
        return new ArrayList<>(tasks);
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
        logActivity(new ActivityImpl(String.format(
                TASK_ADDED_MSG, task.getTaskType(), task.getId(), name)));
    }

    @Override
    public void removeTask(Task task) {
        logActivity(new ActivityImpl(String.format(
                TASK_REMOVED_MSG, task.getTaskType(), task.getId(), name)));
        tasks.remove(task);
    }

    @Override
    public List<Activity> getActivity() {
        return new ArrayList<>(history);
    }

    @Override
    public void logActivity(Activity event) {
        history.add(event);
    }
}
