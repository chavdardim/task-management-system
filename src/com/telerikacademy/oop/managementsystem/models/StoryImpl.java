package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Commentable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;
import com.telerikacademy.oop.managementsystem.models.enums.StorySize;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;

import java.util.ArrayList;
import java.util.List;

public class StoryImpl extends TaskBaseImpl implements Story, Commentable, Assignable {

    public static final String SAME_STORY_PRIORITY = "Story priority is already %s";
    public static final String SAME_STORY_SIZE = "Story size is already %s";
    public static final String SAME_STORY_STATUS = "Story status is already %s";
    public static final String STORY_PRIORITY_CHANGE = "Story priority changed from %s to %s.";
    public static final String STORY_SIZE_CHANGE = "Story size changed from %s to %s.";
    public static final String STORY_CREATED = "Story with id %d created.";
    public static final String STORY_STATUS_CHANGED = "Story status changed from %s to %s.";
    public static final String STORY_ASSIGNED_TO = "Story assigned to %s.";
    public static final String STORY_UNASSIGNED_FROM = "Story unassigned from %s";


    private Priority priority;
    private StorySize storySize;
    private StoryStatus storyStatus;
    private final List<Member> assignees;

    public StoryImpl(int id, String title, String description) {
        super(id, title, description);
        setPriority(Priority.LOW);
        setStorySize(StorySize.SMALL);
        setStoryStatus(StoryStatus.NOTDONE);
        assignees = new ArrayList<>();
        logActivity(new ActivityImpl(String.format(STORY_CREATED, super.getId())));
    }


    private void setPriority(Priority priority) {
        this.priority = priority;
    }

    @Override
    public Priority getPriority() {
        return priority;
    }

    private void setStorySize(StorySize storySize) {
        this.storySize = storySize;
    }

    @Override
    public StorySize getStorySize() {
        return storySize;
    }

    private void setStoryStatus(StoryStatus storyStatus) {
        this.storyStatus = storyStatus;
    }

    @Override
    public String getStatus() {
        return storyStatus.toString();
    }

    @Override
    public TaskType getTaskType() {
        return TaskType.STORY;
    }

    @Override
    public void changePriority(Priority priority) {
        if (priority.equals(this.priority)){
            throw new InvalidUserInputException(String.format(SAME_STORY_PRIORITY, priority));
        }
        logActivity(new ActivityImpl(String.format(STORY_PRIORITY_CHANGE, this.priority, priority)));
        setPriority(priority);
    }

    @Override
    public void changeStorySize(StorySize storySize) {
        if (storySize.equals(this.storySize)){
            throw new InvalidUserInputException(String.format(SAME_STORY_SIZE, storySize));
        }
        logActivity(new ActivityImpl(String.format(STORY_SIZE_CHANGE, this.storySize, storySize)));
        setStorySize(storySize);
    }

    @Override
    public void changeStoryStatus(StoryStatus storyStatus) {
        if (storyStatus.equals(this.storyStatus)){
            throw new InvalidUserInputException(String.format(SAME_STORY_STATUS, storyStatus));
        }
        logActivity(new ActivityImpl(String.format(STORY_STATUS_CHANGED, this.storyStatus, storyStatus)));
        setStoryStatus(storyStatus);
    }

    @Override
    public void setAssignees(List<Member> members) {
        logActivity(new ActivityImpl(String.format(STORY_ASSIGNED_TO, members.toString())));
        assignees.addAll(members);
    }

    @Override
    public void removeAssignees(List<Member> members) {
        logActivity(new ActivityImpl(String.format(STORY_UNASSIGNED_FROM, members.toString())));
        assignees.removeAll(members);
    }

    @Override
    public List<Member> getAssignees() {

        return new ArrayList<>(assignees);
    }

    @Override
    public String getAdditionalInfo() {
        return String.format("Priority: %s%n" +
                        "Size: %s%n" +
                        "Status: %s%n" +
                        "Assignee: %s",
                priority,
                storySize,
                storyStatus,
                assignees);
    }
}
