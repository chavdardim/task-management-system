package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;

public class FeedbackImpl extends TaskBaseImpl implements Feedback {

    public static final String SAME_FEEDBACK_RATING = "Feedback rating is already %s";
    public static final String SAME_FEEDBACK_STATUS = "Feedback status is already %s";
    public static final String FEEDBACK_RATING_CHANGE = "Feedback rating changed from %s to %s.";
    public static final String FEEDBACK_STATUS_CHANGE = "Feedback status changed from %s to %s.";
    public static final String FEEDBACK_CREATED = "Feedback with id %d created.";

    private int rating;
    private FeedbackStatus feedbackStatus;

    public FeedbackImpl(int id, String title, String description) {
        super(id, title, description);
        setRating(rating = 1);
        setStatus(FeedbackStatus.NEW);
        logActivity(new ActivityImpl(String.format(FEEDBACK_CREATED, id)));
    }

    private void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public int getRating() {
        return rating;
    }

    private void setStatus(FeedbackStatus feedbackStatus) {
        this.feedbackStatus = feedbackStatus;
    }

    @Override
    public String getStatus() {
        return feedbackStatus.toString();
    }

    @Override
    public TaskType getTaskType() {
        return TaskType.FEEDBACK;
    }

    @Override
    public void changeRating(int newRating) {
        if (newRating == rating){
            throw new InvalidUserInputException(String.format(SAME_FEEDBACK_RATING, rating));
        }
        logActivity(new ActivityImpl(String.format(FEEDBACK_RATING_CHANGE, this.rating, newRating)));
        setRating(newRating);
    }

    @Override
    public void changeFeedbackStatus(FeedbackStatus feedbackStatus) {
        if (feedbackStatus.equals(this.feedbackStatus)){
            throw new InvalidUserInputException(String.format(SAME_FEEDBACK_STATUS,feedbackStatus));
        }
        logActivity(new ActivityImpl(String.format(
                FEEDBACK_STATUS_CHANGE, this.feedbackStatus, feedbackStatus)));
        setStatus(feedbackStatus);
    }

    @Override
    public String getAdditionalInfo() {
        return  String.format("Rating: %s%n" +
                        "Status: %s",
                rating,
                feedbackStatus);
    }
}
