package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.Manageable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Nameable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Printable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.LogActivityAble;

import java.util.List;

public interface Member extends Manageable {

    List<Task> getTasks();

    void addTask(Task task);

    void removeTask(Task task);

}
