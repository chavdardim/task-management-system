package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.LogActivityAble;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Manageable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Nameable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Printable;

import java.util.List;

public interface Team extends Manageable {

    List<Member> getMemberList();

    void addMember(Member member);

    List<Board> getBoards();

    void addBoard(Board board);

}
