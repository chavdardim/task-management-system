package com.telerikacademy.oop.managementsystem.models.contracts.models;

public interface Activity {

    String viewInfo();

}
