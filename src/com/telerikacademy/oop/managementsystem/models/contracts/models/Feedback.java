package com.telerikacademy.oop.managementsystem.models.contracts.models;

import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;

public interface Feedback extends Task {

    int getRating();

    void changeRating(int newRating);

    void changeFeedbackStatus(FeedbackStatus feedbackStatus);
}
