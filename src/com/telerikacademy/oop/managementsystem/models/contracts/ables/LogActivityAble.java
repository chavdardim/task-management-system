package com.telerikacademy.oop.managementsystem.models.contracts.ables;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;

import java.util.List;

public interface LogActivityAble {

    List<Activity> getActivity();

    void logActivity(Activity event);

}
