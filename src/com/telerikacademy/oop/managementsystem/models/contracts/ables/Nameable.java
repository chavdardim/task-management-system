package com.telerikacademy.oop.managementsystem.models.contracts.ables;

public interface Nameable {

    public String getName();
}
