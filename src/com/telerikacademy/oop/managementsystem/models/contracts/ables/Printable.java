package com.telerikacademy.oop.managementsystem.models.contracts.ables;

public interface Printable {

    String getAsString();
}
