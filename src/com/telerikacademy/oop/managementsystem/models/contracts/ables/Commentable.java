package com.telerikacademy.oop.managementsystem.models.contracts.ables;

import java.util.List;

public interface Commentable {

    void addComment(String comment);

    List<String> getComments();

}
