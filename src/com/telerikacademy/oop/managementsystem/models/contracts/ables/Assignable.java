package com.telerikacademy.oop.managementsystem.models.contracts.ables;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.enums.Priority;

import java.util.List;

public interface Assignable extends Printable {

    void setAssignees(List<Member> members);

    void removeAssignees(List<Member> members);

    List<Member> getAssignees();

}
