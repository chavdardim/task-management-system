package com.telerikacademy.oop.managementsystem.models.contracts.ables;

public interface Identifiable {

    int getId();
}
