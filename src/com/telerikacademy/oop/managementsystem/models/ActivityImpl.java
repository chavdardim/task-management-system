package com.telerikacademy.oop.managementsystem.models;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Activity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class ActivityImpl implements Activity {

    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss");

    private final String description;
    private final LocalDateTime timestamp;

    public ActivityImpl(String description) {
        this.description = description;
        this.timestamp = LocalDateTime.now();
    }

    @Override
    public String viewInfo(){
        return String.format("[%s] %s", timestamp.format(formatter), description);
    }
}
