package com.telerikacademy.oop.managementsystem.models.enums;

public enum StoryStatus {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case NOTDONE:
                return "Notdone";
            case INPROGRESS:
                return "Inprogress";
            case DONE:
                return "Done";
            default:
                throw new IllegalArgumentException();
        }
    }
}
