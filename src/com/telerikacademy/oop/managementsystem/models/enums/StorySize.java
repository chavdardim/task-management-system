package com.telerikacademy.oop.managementsystem.models.enums;

public enum StorySize {
    SMALL,
    MEDIUM,
    LARGE
}
