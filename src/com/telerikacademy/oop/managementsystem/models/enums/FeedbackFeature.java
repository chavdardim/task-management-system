package com.telerikacademy.oop.managementsystem.models.enums;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;

public enum FeedbackFeature implements Features<Feedback> {
    RATING {
        @Override
        public void change(Feedback feedback, String newValue) {
            feedback.changeRating(ParsingHelpers.tryParseInt(newValue,
                    "Invalid input! Please enter a valid number for rating."));
        }
    },
    FEEDBACKSTATUS {
        @Override
        public void change(Feedback feedback, String newValue) {
            feedback.changeFeedbackStatus(ParsingHelpers.tryParseEnum(newValue, FeedbackStatus.class));
        }
    };

    @Override
    public String toString() {
        return switch (this) {
            case RATING -> "Rating";
            case FEEDBACKSTATUS -> "FeedbackStatus";
        };
    }
}
