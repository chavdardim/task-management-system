package com.telerikacademy.oop.managementsystem.models.enums;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;

public interface Features<E> {
    void change(E bug, String newValue);
}
