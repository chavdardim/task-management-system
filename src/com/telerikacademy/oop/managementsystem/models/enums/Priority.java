package com.telerikacademy.oop.managementsystem.models.enums;

public enum Priority {
    LOW,
    MEDIUM,
    HIGH
}
