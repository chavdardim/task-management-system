package com.telerikacademy.oop.managementsystem.models.enums;

import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;

public enum StoryFeature implements Features<Story> {
    PRIORITY {
        @Override
        public void change(Story story, String newValue) {
            story.changePriority(ParsingHelpers.tryParseEnum(newValue, Priority.class));
        }
    },
    SIZE {
        @Override
        public void change(Story story, String newValue) {
            story.changeStorySize(ParsingHelpers.tryParseEnum(newValue, StorySize.class));
        }
    },
    STATUS {
        @Override
        public void change(Story story, String newValue) {
            story.changeStoryStatus(ParsingHelpers.tryParseEnum(newValue, StoryStatus.class));
        }
    };

    @Override
    public String toString() {
        return switch (this) {
            case PRIORITY -> "Priority";
            case SIZE -> "Size";
            case STATUS -> "Status";
        };
    }
}
