package com.telerikacademy.oop.managementsystem.exceptions;

public class InvalidUserInputException extends RuntimeException{
    public InvalidUserInputException(String message) {
        super(message);
    }
}
