package com.telerikacademy.oop.managementsystem.exceptions;

public class ElementNotFoundException extends RuntimeException{
    public ElementNotFoundException(String message) {
        super(message);
    }
}
