package com.telerikacademy.oop.managementsystem;

import com.telerikacademy.oop.managementsystem.core.ManagementSystemEngineImpl;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemEngine;
import com.telerikacademy.oop.managementsystem.models.BoardImpl;
import com.telerikacademy.oop.managementsystem.models.BugImpl;
import com.telerikacademy.oop.managementsystem.models.StoryImpl;
import com.telerikacademy.oop.managementsystem.models.TeamImpl;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StartUp {
    public static void main(String[] args) {
        ManagementSystemEngine engine= new ManagementSystemEngineImpl();
        engine.start();
    }
}
