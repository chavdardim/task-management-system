package com.telerikacademy.oop.managementsystem.utils;

import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Nameable;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Printable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class ListingHelpers {

    public static final String JOIN_DELIMITER = "--------------------";

    public static String tasksToString(List<Task> tasks) {
        return elementsToString(tasks);
    }

    public static String assignablesToString(List<Assignable> assignables) {

        return elementsToString(assignables);
    }

    public static <T extends Printable> String elementsToString(List<T> elements) {
        List<String> result = elements
                .stream()
                .map(Printable::getAsString)
                .collect(Collectors.toList());

        return String.join(JOIN_DELIMITER + System.lineSeparator(), result).trim();
    }

    public static String stringToList(List<String> elements) {
        StringBuilder result = new StringBuilder();
        elements.forEach(element -> result.append("\n").append(element));

        return String.join(JOIN_DELIMITER, result);
    }

    public static String membersToList(List<Member> teamMembers, String teamName) {
        return printListOfItems(teamMembers, String.format("Members of %s team: %n", teamName));
    }

    public static String freePeopleToList(List<Member> freePeople) {
        return printListOfItems(freePeople, "People: \n");
    }

    public static <T extends Nameable> String printListOfItems(List<T> items, String title){
        int num = 1;
        StringBuilder names = new StringBuilder();
        names.append(title);
        for (T item : items) {
            names.append(String.format("%d. %s%n", num++, item.getName()));
        }
        return names.substring(0, names.length() - 1);
    }

}
