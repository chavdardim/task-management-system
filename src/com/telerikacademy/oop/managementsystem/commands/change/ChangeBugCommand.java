package com.telerikacademy.oop.managementsystem.commands.change;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.enums.BugFeature;

import java.util.List;

public class ChangeBugCommand extends BaseChangeCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public ChangeBugCommand(ManagementSystemRepository managementSystemRepository) {
        super(managementSystemRepository);
    }

    @Override
    public String execute(List<String> arguments) {
        return super.execute(EXPECTED_NUMBER_OF_ARGUMENTS, arguments, Bug.class, BugFeature.class);
    }
}
