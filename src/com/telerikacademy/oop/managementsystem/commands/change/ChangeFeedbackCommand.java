package com.telerikacademy.oop.managementsystem.commands.change;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackFeature;

import java.util.List;

public class ChangeFeedbackCommand extends BaseChangeCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public ChangeFeedbackCommand(ManagementSystemRepository managementSystemRepository) {
        super(managementSystemRepository);
    }

    @Override
    public String execute(List<String> arguments) {
        return super.execute(EXPECTED_NUMBER_OF_ARGUMENTS, arguments, Feedback.class, FeedbackFeature.class);
    }
}
