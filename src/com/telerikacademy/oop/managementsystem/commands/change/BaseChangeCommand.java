package com.telerikacademy.oop.managementsystem.commands.change;

import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.enums.Features;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;

import static java.lang.String.format;

public class BaseChangeCommand {
    private static final String TASK_CHANGED_SUCCESS_MESSAGE = "%s with ID %d %s changed to %s.";
    public static final String INVALID_NUMBER = "Invalid input! Please enter a valid number.";

    private final ManagementSystemRepository managementSystemRepository;

    public BaseChangeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    public <T extends Task, E extends Enum<E> & Features<T>> String execute(
            final int expectedNumberOfArguments,
            List<String> arguments,
            Class<T> taskClass,
            Class<E> featureClass) {
        ValidationHelpers.validateArgumentsCount(arguments, expectedNumberOfArguments);
        int id = ParsingHelpers.tryParseInt(arguments.get(0), INVALID_NUMBER);
        Features<T> feature = ParsingHelpers.tryParseEnum(arguments.get(1), featureClass);
        String newValue = arguments.get(2);
        T task = managementSystemRepository.findItemById(id, taskClass);
        feature.change(task, newValue);
        return format(TASK_CHANGED_SUCCESS_MESSAGE,
                taskClass.getSimpleName().toLowerCase(),
                id,
                feature,
                newValue);
    }
}
