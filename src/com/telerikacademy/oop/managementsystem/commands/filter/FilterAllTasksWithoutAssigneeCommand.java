package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterAllTasksWithoutAssigneeCommand implements Command {

    public static final String NO_BUGS_OR_STORIES = "There are not bugs and stories.";
    private final ManagementSystemRepository managementSystemRepository;

    public FilterAllTasksWithoutAssigneeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

        List<Assignable> tasks = new ArrayList<>();
        tasks.addAll(managementSystemRepository.getAllBugs());
        tasks.addAll(managementSystemRepository.getAllStories());

        if (tasks.isEmpty()){
            throw new IllegalArgumentException(NO_BUGS_OR_STORIES);
        }

        List<Assignable> result = tasks
                        .stream()
                        .filter(task -> task.getAssignees().isEmpty())
                        .collect(Collectors.toList());

        return String.format("--All tasks without assignee--%n") + ListingHelpers.assignablesToString(result);
    }
}
