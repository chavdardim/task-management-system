package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterTaskTypeByStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;
    public static final String INVALID_STATUS_INPUT = "Not a valid Status. Please enter corresponding status";
    public static final String NO_TASKS_WITH_STATUS = "--No %s with status %s";

    private final ManagementSystemRepository managementSystemRepository;

    public FilterTaskTypeByStatusCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String taskType = arguments.get(0);
        String statusInput = arguments.get(1);

        return showTasksWithAssigneeAndStatus(taskType, statusInput);
    }

    private String showTasksWithAssigneeAndStatus(String taskType, String statusInput) {

        List<Task> allTasks = managementSystemRepository.getAllTasks();


        if(managementSystemRepository.getAllTasks().isEmpty()){
            throw new IllegalArgumentException("There are not tasks created");
        }
        StatusValidator.checkIfStatusIsValid(statusInput, INVALID_STATUS_INPUT);
        List<Task> filteredTasks = allTasks
                .stream()
                .filter(task -> task.getTaskType().toString().equalsIgnoreCase(taskType))
                .filter(task -> task.getStatus().equalsIgnoreCase(statusInput))
                .collect(Collectors.toList());

        if (filteredTasks.isEmpty()){
            throw new InvalidUserInputException(String.format(NO_TASKS_WITH_STATUS,taskType, statusInput));
        }
        return String.format("--All %ss filtered by %s--%n",  taskType, statusInput) +
            ListingHelpers.tasksToString(filteredTasks);
    }
}



