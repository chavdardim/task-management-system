package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class FilterAllTasksByAssigneeCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String NO_TASKS_ASSIGNED = "%s not assigned to any task";
    public static final String FILTER_TASKS_BY_ASSIGNEE = "--All tasks filtered by ASSIGNEE %s--\n";


    private final ManagementSystemRepository managementSystemRepository;

    public FilterAllTasksByAssigneeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String assigneeName = arguments.get(0);


        List<Assignable> tasks = new ArrayList<>();
        tasks.addAll(managementSystemRepository.getAllBugs());
        tasks.addAll(managementSystemRepository.getAllStories());

        List<Assignable> result = tasks
                .stream()
                .filter(assignable -> assignable.getAssignees().contains(
                        managementSystemRepository.findMemberByName(assigneeName)))
                .collect(Collectors.toList());

        if (result.isEmpty()) {
            throw new IllegalArgumentException(String.format(NO_TASKS_ASSIGNED, assigneeName));
        }

        return String.format(FILTER_TASKS_BY_ASSIGNEE, assigneeName) +
                ListingHelpers.assignablesToString(result);

    }
}
