package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.ables.Assignable;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FilterTaskTypeByAssigneeCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String NO_TASKS_ASSIGNED = "%s not assigned to any task";
    public static final String NO_ASSIGNED_TASKS = "There are no %s assigned to %s.";
    private final ManagementSystemRepository managementSystemRepository;

    public FilterTaskTypeByAssigneeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        TaskType taskType = ParsingHelpers.tryParseEnum(arguments.get(0), TaskType.class);
        String assigneeName = arguments.get(1);

        List<Assignable> bugsOrStories = new ArrayList<>();

        switch (taskType){
            case BUG:
                bugsOrStories.addAll(managementSystemRepository.getAllBugs());
                return showAllAssignedBugs(bugsOrStories, assigneeName, taskType);
            case STORY:
                bugsOrStories.addAll(managementSystemRepository.getAllStories());
                return showAllAssignedBugs(bugsOrStories, assigneeName, taskType);
            default:
                throw new InvalidUserInputException("Feedback cannot be assigned.");
        }
    }

    public String showAllAssignedBugs(List<Assignable> bugsOrStories, String assigneeName, TaskType taskType){

        if (bugsOrStories.isEmpty()){
            throw new IllegalArgumentException(String.format(NO_ASSIGNED_TASKS, taskType, assigneeName));
        }

        Member assignee = managementSystemRepository.findMemberByName(assigneeName);

        List<Assignable> result = bugsOrStories
                .stream()
                .filter(assignable -> assignable.getAssignees().contains(assignee))
                .collect(Collectors.toList());

        if (result.isEmpty()){
            throw new IllegalArgumentException(String.format(NO_TASKS_ASSIGNED, assigneeName));
        }
        return ListingHelpers.assignablesToString(result);
    }
}
