package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StatusValidator {

    public static void checkIfStatusIsValid(String statusInput, String message){
        List<String> allStatus = new ArrayList<>();

        Arrays.stream(BugStatus.values()).forEach(status -> allStatus.add(status.toString()));
        Arrays.stream(StoryStatus.values()).forEach(status -> allStatus.add(status.toString()));
        Arrays.stream(FeedbackStatus.values()).forEach(status -> allStatus.add(status.toString()));

        boolean validStatus = allStatus
                .stream()
                .anyMatch(status -> status.equalsIgnoreCase(statusInput));

        if (!validStatus){
            throw new InvalidUserInputException(message);
        }
    }
}
