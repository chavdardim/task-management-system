package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.*;
import com.telerikacademy.oop.managementsystem.models.enums.BugStatus;
import com.telerikacademy.oop.managementsystem.models.enums.FeedbackStatus;
import com.telerikacademy.oop.managementsystem.models.enums.StoryStatus;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FilterAllTasksByStatusCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String INVALID_STATUS_INPUT = "Not a valid Status. Please enter corresponding status";
    public static final String NO_TASKS_WITH_STATUS = "--No tasks with status %s";

    private final ManagementSystemRepository managementSystemRepository;

    public FilterAllTasksByStatusCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String statusInput = arguments.get(0);

        List<Task> allTasks = managementSystemRepository.getAllTasks();
        StatusValidator.checkIfStatusIsValid(statusInput, INVALID_STATUS_INPUT);
        if (allTasks.isEmpty()){
            throw new IllegalArgumentException("There are no tasks ");
        }

        List<Task> filteredTasks = allTasks
                .stream()
                .filter(task -> task.getStatus().equalsIgnoreCase(statusInput))
                .collect(Collectors.toList());

        if (filteredTasks.isEmpty()){
            throw new InvalidUserInputException(String.format(NO_TASKS_WITH_STATUS,statusInput));
        }
        return String.format("--All tasks filtered by %s--%n", statusInput) +
                ListingHelpers.tasksToString(filteredTasks);
    }
}
