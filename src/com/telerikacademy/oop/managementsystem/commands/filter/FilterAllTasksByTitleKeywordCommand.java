package com.telerikacademy.oop.managementsystem.commands.filter;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;
import java.util.stream.Collectors;

public class FilterAllTasksByTitleKeywordCommand implements Command {

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;

    private final ManagementSystemRepository managementSystemRepository;

    public FilterAllTasksByTitleKeywordCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        String keyWord = arguments.get(0);

        List<Task> allTasks = managementSystemRepository.getAllTasks();

        if (allTasks.isEmpty()){
            throw new IllegalArgumentException("There are no tasks created.");
        }

        return String.format("--All tasks filtered by \"%s\" keyword--%n", keyWord) + allTasks
                .stream()
                .filter(task -> task.getTitle().contains(keyWord))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n"));
    }
}
