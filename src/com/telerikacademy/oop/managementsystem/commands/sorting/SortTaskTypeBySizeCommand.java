package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortTaskTypeBySizeCommand implements Command {

    private final ManagementSystemRepository managementSystemRepository;

    public SortTaskTypeBySizeCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

        List<Story> allStories = managementSystemRepository.getAllStories();
        if (allStories.isEmpty()){
            throw new ElementNotFoundException("There are no stories.");
        }
        return "--Sorted Stories by SIZE--\n" + allStories
                .stream()
                .sorted(Comparator.comparing(Story::getStorySize))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n"));

    }

}
