package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortAllTasksByTitleCommand implements Command {

    private final ManagementSystemRepository managementSystemRepository;

    public SortAllTasksByTitleCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

        List<Task> allTasks = managementSystemRepository.getAllTasks();

        if(allTasks.isEmpty()){
            throw new IllegalArgumentException("There are no members.");
        }

        return "--Sorted by TITLE--\n" + allTasks
                .stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n"));
    }
}
