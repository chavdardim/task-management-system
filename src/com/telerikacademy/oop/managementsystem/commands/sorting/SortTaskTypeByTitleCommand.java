package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortTaskTypeByTitleCommand implements Command {
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;
    public static final String INVALID_TASKTYPE = "Not a valid TaskType. Please enter: Bug, Story or Feedback";

    private final ManagementSystemRepository managementSystemRepository;

    public SortTaskTypeByTitleCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        TaskType taskType = ParsingHelpers.tryParseEnum(arguments.get(0).toUpperCase(), TaskType.class);

        if (managementSystemRepository.getAllTasks().isEmpty()){
            throw new ElementNotFoundException("There are no tasks.");
        }

        switch (taskType) {
            case BUG:
                List<Bug> allBugs = managementSystemRepository.getAllBugs();
                return sortTaskType(allBugs, "Bugs");
            case STORY:
                List<Story> allStories = managementSystemRepository.getAllStories();
                return sortTaskType(allStories, "Stories");
            case FEEDBACK:
                List<Feedback> allFeedbacks = managementSystemRepository.getAllFeedbacks();
                return sortTaskType(allFeedbacks, "Feedbacks");
            default:
                throw new InvalidUserInputException(INVALID_TASKTYPE);
        }
    }

    private String sortTaskType(List<? extends Task> taskTypeList, String type) {

        return String.format("--Sorted %s by TITLE--\n" + taskTypeList
                .stream()
                .sorted(Comparator.comparing(Task::getTitle))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n")), type);
    }
}
