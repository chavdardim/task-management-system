package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortTaskTypeBySeverityCommand implements Command {

    private final ManagementSystemRepository managementSystemRepository;

    public SortTaskTypeBySeverityCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {

            List<Bug> allBugs = managementSystemRepository.getAllBugs();
            if (allBugs.isEmpty()){
                throw new ElementNotFoundException("There are no bugs.");
            }
            return "--Sorted Bugs by SEVERITY--\n" + allBugs
                    .stream()
                    .sorted(Comparator.comparing(Bug::getSeverity))
                    .map(Task::getImportantInfo)
                    .collect(Collectors.joining("\n"));
        }
    }

