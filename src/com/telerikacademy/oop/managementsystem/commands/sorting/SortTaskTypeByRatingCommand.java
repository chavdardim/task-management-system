package com.telerikacademy.oop.managementsystem.commands.sorting;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Feedback;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortTaskTypeByRatingCommand implements Command {

    private final ManagementSystemRepository managementSystemRepository;

    public SortTaskTypeByRatingCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        List<Feedback> allFeedbacks = managementSystemRepository.getAllFeedbacks();
        if(allFeedbacks.isEmpty()){
            throw new ElementNotFoundException("There are no feedbacks.");
        }
        return "--Sorted Feedbacks by RATING--\n" + allFeedbacks
                .stream()
                .sorted(Comparator.comparing(Feedback::getRating))
                .map(Task::getImportantInfo)
                .collect(Collectors.joining("\n"));
    }
}
