package com.telerikacademy.oop.managementsystem.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.enums.*;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class AssignAllStoriesCommand implements Command {

    private static final String STORY_ALREADY_ASSIGNED = "Stories with type %s is already assigned to member/s with name %s.";
    public static final String STORY_WITH_TYPE_ASSIGNED = "All stories with type %s are assigned to %s.";
    public static final String NO_STORY_OF_TYPE = "No stories with this type: %s.";
    public static final String NO_STORIES_CREATED = "There are no story created of any kind.";

    private final ManagementSystemRepository managementSystemRepository;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 3;

    public AssignAllStoriesCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        List<String> memberNames = Arrays.asList(arguments.get(0).split(","));
        String storyFeature = arguments.get(1).toUpperCase(Locale.ROOT);
        String featureType = arguments.get(2).toUpperCase(Locale.ROOT);

        List<Story> storiesToBeAssigned = getAllBugsOfUnknownType(storyFeature, featureType);
        List<Member> members = new ArrayList<>();
        memberNames.forEach(memberName -> members.add(managementSystemRepository.findMemberByName(memberName)));

        throwsExceptionIfBugAssignedToMember(storiesToBeAssigned, members, featureType);

        return assignMembersAndBugs(storiesToBeAssigned, members, featureType);

    }

    private List<Story> getAllBugsOfUnknownType(String storyFeature, String featureType){

        List<Story> allStories = managementSystemRepository.getAllStories();
        List<Story> storiesToBeAssigned = new ArrayList<>();

        if (allStories.isEmpty()){
            throw new IllegalArgumentException(NO_STORIES_CREATED);
        }

        switch (storyFeature){
            case "PRIORITY":
                Priority priority = ParsingHelpers.tryParseEnum(featureType, Priority.class);
                allStories.forEach(story -> {
                    if (story.getPriority().equals(priority)) {
                        storiesToBeAssigned.add(story);
                    }
                });
                return storiesToBeAssigned;
            case "SIZE":
                StorySize storySize = ParsingHelpers.tryParseEnum(featureType, StorySize.class);
                allStories.forEach(story -> {
                    if (story.getStorySize().equals(storySize)) {
                        storiesToBeAssigned.add(story);
                    }
                });
                return storiesToBeAssigned;
            case "STATUS":
                allStories.forEach(story -> {
                    if (story.getStatus().equalsIgnoreCase(featureType)) {
                        storiesToBeAssigned.add(story);
                    }
                });
                return storiesToBeAssigned;
            default:
                throw new InvalidUserInputException(String.format(NO_STORY_OF_TYPE, featureType));
        }
    }

    private String assignMembersAndBugs(List<Story> storiesToBeAssigned, List<Member> members, String type){
        storiesToBeAssigned
                .forEach(bug -> members.
                        forEach(member -> member.addTask(bug)));
        storiesToBeAssigned.forEach(story -> story.setAssignees(members));
        return String.format(STORY_WITH_TYPE_ASSIGNED, type, members);
    }

    private void throwsExceptionIfBugAssignedToMember(List<Story> storiesToBeAssigned, List<Member> members, String type) {

        boolean isAssigned = storiesToBeAssigned
                .stream()
                .anyMatch(story ->story.getAssignees()
                        .stream()
                        .anyMatch(assignee -> members.stream()
                                .anyMatch(member -> member.getName().equals(assignee.getName()))));
        if (isAssigned){
            throw new IllegalArgumentException(String.format(STORY_ALREADY_ASSIGNED, type, members));
        }
    }

}
