package com.telerikacademy.oop.managementsystem.commands.assign;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Story;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.utils.ParsingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;

public class UnassignCommand implements Command {
    private static final String INVALID_TASK_ID = "Task ID should be a valid number";
    private static final String BUG_UNASSIGN_SUCCESS = "Bug with ID %d unassigned from %s.";
    private static final String STORY_UNASSIGN_SUCCESS = "Story with ID %d unassigned from %s.";
    private static final String MEMBER_NOT_ASSIGNED = "%s is not assigned to bug with ID %d.";

    private final ManagementSystemRepository managementSystemRepository;

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 2;


    public UnassignCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);

        int id = ParsingHelpers.tryParseInt(arguments.get(0), INVALID_TASK_ID);
        List<String> memberName = Arrays.asList(arguments.get(1).split(","));

        return assignTask(id, memberName);
    }

    private String assignTask(int id, List<String> memberNames) {

        List<Member> membersToUnassign = new ArrayList<>();
        memberNames.forEach(memberName -> membersToUnassign.add(managementSystemRepository.findMemberByName(memberName)));

        Task task = throwsExceptionIfMemberNotAssigned(id, membersToUnassign, memberNames);

        switch(task.getTaskType()){
            case BUG:
                Bug bug = managementSystemRepository.findBugById(id);
                bug.removeAssignees(membersToUnassign);
                membersToUnassign.forEach(member -> member.removeTask(bug));
                return format(BUG_UNASSIGN_SUCCESS, id, memberNames);
            case STORY:
                Story story = managementSystemRepository.findStoryById(id);
                story.removeAssignees(membersToUnassign);
                membersToUnassign.forEach(member -> member.removeTask(story));
                return format(STORY_UNASSIGN_SUCCESS, id, memberNames);
            default:
                throw new InvalidUserInputException("Feedback cannot be unassigned");
        }
    }

    private Task throwsExceptionIfMemberNotAssigned(int id, List<Member> members, List<String> memberNames){
        Task task = managementSystemRepository.findTaskById(id);
        boolean isMemberAssigned = members.stream().anyMatch(member -> member.getTasks().contains(task));
        if (!isMemberAssigned){
            throw new InvalidUserInputException(String.format(MEMBER_NOT_ASSIGNED, memberNames, id));
        }
        return task;
    }
}
