package com.telerikacademy.oop.managementsystem.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;


public class ShowTeamMembersCommand implements Command {
    private static final String TEAM_DOES_NOT_EXIST = "Team with name %s doesn't exist.";
    private static final String TEAM_WITH_NO_MEMBERS = "Team %s doesn't have any members";

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;


    private final ManagementSystemRepository managementSystemRepository;

    public ShowTeamMembersCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = arguments.get(0);

        return showMembers(teamName);
    }

    private String showMembers(String teamName) {
        if(!managementSystemRepository.teamExists(teamName)){
            throw new IllegalArgumentException(String.format(TEAM_DOES_NOT_EXIST, teamName));
        }
        if (managementSystemRepository.findTeamByName(teamName).getMemberList().isEmpty()){
            throw new IllegalArgumentException(String.format(TEAM_WITH_NO_MEMBERS, teamName));
        }

        Team team = managementSystemRepository.findTeamByName(teamName);
        List<Member> teamMembers = team.getMemberList();


        return ListingHelpers.membersToList(teamMembers, teamName);
    }
}
