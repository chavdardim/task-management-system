package com.telerikacademy.oop.managementsystem.commands.show;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.utils.ListingHelpers;

import java.util.List;


public class ShowPeopleCommand implements Command {

    public static final String NO_PEOPLE_WITHOUT_TEAM = "There are no people without team.";
    private final ManagementSystemRepository managementSystemRepository;

    public ShowPeopleCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        if (managementSystemRepository.getFreePeople().isEmpty()){
            throw new IllegalArgumentException(NO_PEOPLE_WITHOUT_TEAM);
        }

        List<Member> freePeople = managementSystemRepository.getFreePeople();

        return ListingHelpers.freePeopleToList(freePeople);
    }
}
