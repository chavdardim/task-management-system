package com.telerikacademy.oop.managementsystem.commands.creation;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Notification;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.dtos.CommentDto;

import java.util.List;

public class AddComment {

    public static class CommandImpl implements Command<CommandResult>, Notification {

        public final ManagementSystemRepository managementSystemRepository;
        public final List<String> parameters;
        public CommentDto commentDto;

        public CommandImpl(ManagementSystemRepository managementSystemRepository, List<String> parameters) {
            this.managementSystemRepository = managementSystemRepository;
            this.parameters = parameters;
        }
    }

    public static class Handler implements Command.Handler<CommandImpl, CommandResult> {

        public static final String COMMENT_SUCCESS_MSG = "%s with ID %d successfully commented.";

        @Override
        public CommandResult handle(CommandImpl request) {
            String message = addComment(request.commentDto.taskId(), request.commentDto.comment(), request.managementSystemRepository);
            return new CommandResult(message);
        }

        private String addComment(int id, String comment, ManagementSystemRepository managementSystemRepository) {
            Task task = managementSystemRepository.findTaskById(id);
            String type = task.getTaskType().toString();
            task.addComment(comment);
            return String.format(COMMENT_SUCCESS_MSG, type, id);
        }
    }

}

