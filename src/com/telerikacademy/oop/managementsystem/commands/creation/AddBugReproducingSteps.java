package com.telerikacademy.oop.managementsystem.commands.creation;

import an.awesome.pipelinr.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Bug;
import com.telerikacademy.oop.managementsystem.models.dtos.BugReproducingStepsDto;

import java.util.List;

public class AddBugReproducingSteps{
    public static class CommandImpl implements Command<CommandResult>{
        public final ManagementSystemRepository managementSystemRepository;
        public final List<String> parameters;
        public BugReproducingStepsDto bugReproducingStepsDto;

        public CommandImpl(ManagementSystemRepository managementSystemRepository, List<String> parameters) {
            this.managementSystemRepository = managementSystemRepository;
            this.parameters = parameters;
        }
    }
    public static class CommandHanler implements Command.Handler<CommandImpl, CommandResult>{
        private static final String REPRODUCING_STEPS_ADDED = "Reproducing steps for Bug with ID %d added.";

        @Override
        public CommandResult handle(CommandImpl request) {
            String message = addSteps(
                    request.bugReproducingStepsDto.reproducingSteps(),
                    request.bugReproducingStepsDto.bugId(),
                    request.managementSystemRepository);
            return new CommandResult(message);
        }

        private String addSteps(String steps, int id, ManagementSystemRepository managementSystemRepository) {
            Bug bug = managementSystemRepository.findBugById(id);
            bug.addReproducingSteps(steps);
            return String.format(REPRODUCING_STEPS_ADDED, id);
        }
    }

}
