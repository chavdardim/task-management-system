package com.telerikacademy.oop.managementsystem.commands.creation;

import an.awesome.pipelinr.Command;
import an.awesome.pipelinr.Notification;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.events.CreatePersonCompletedNotification;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.dtos.PersonDto;

import java.util.List;

import static java.lang.String.format;

public class CreatePerson {
    public static class CommandImpl implements Command<CommandResult>, Notification {
        public final ManagementSystemRepository managementSystemRepository;
        public final List<String> parameters;
        public CommandResult executionResult;
        public PersonDto personDto;

        public CommandImpl(ManagementSystemRepository managementSystemRepository, List<String> parameters) {
            this.managementSystemRepository = managementSystemRepository;
            this.parameters = parameters;
        }
    }

    public static class CommandHandler implements Command.Handler<CommandImpl, CommandResult> {

        private static final String PERSON_CREATED_SUCCESS_MESSAGE = "User %s created.";

        @Override
        public CommandResult handle(CommandImpl request) {
            String message = createPerson(request.personDto.personName(), request.managementSystemRepository);
            request.executionResult = new CommandResult(message);
            request.managementSystemRepository.addNotification(new CreatePersonCompletedNotification(new CommandResult(message)));
            return request.executionResult;
        }

        private String createPerson(String name, ManagementSystemRepository managementSystemRepository) {
            managementSystemRepository.addPerson(name);
            return format(PERSON_CREATED_SUCCESS_MESSAGE, name);
        }
    }
}
