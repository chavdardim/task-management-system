package com.telerikacademy.oop.managementsystem.commands.creation;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.InvalidUserInputException;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;

import static java.lang.String.format;

public class CreateTeamCommand implements Command {

    private static final String TEAM_ALREADY_EXIST = "Team with name %s already exist.";
    private static final String TEAM_CREATE_SUCCESS_MESSAGE = "Team %s created.";

    private final ManagementSystemRepository managementSystemRepository;

    public CreateTeamCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 1;


    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String teamName = arguments.get(0);

        return createTeam(teamName);
    }

    private String createTeam(String teamName) {
        if (managementSystemRepository.teamExists(teamName)){
            throw new InvalidUserInputException(format(TEAM_ALREADY_EXIST, teamName));
        }
        managementSystemRepository.addTeam(teamName);

        return format(TEAM_CREATE_SUCCESS_MESSAGE, teamName);
    }
}
