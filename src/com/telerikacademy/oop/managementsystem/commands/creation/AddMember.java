package com.telerikacademy.oop.managementsystem.commands.creation;

import an.awesome.pipelinr.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Member;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.models.dtos.MemberDto;

import java.util.List;

import static java.lang.String.format;

public class AddMember {
    public static class CommandImpl implements Command<CommandResult> {
        public final ManagementSystemRepository managementSystemRepository;
        public final List<String> parameters;
        public MemberDto memberDto;

        public CommandImpl(ManagementSystemRepository managementSystemRepository, List<String> parameters) {
            this.managementSystemRepository = managementSystemRepository;
            this.parameters = parameters;
        }
    }

    public static  class CommandHandler implements Command.Handler<CommandImpl, CommandResult> {
        private static final String MEMBER_SUCCESS_MESSAGE = "%s is added to %s team";

        @Override
        public CommandResult handle(CommandImpl request) {
            return new CommandResult(addMember(request.memberDto.personName(), request.memberDto.teamName(), request.managementSystemRepository));
        }

        private String addMember(String personName, String teamName, ManagementSystemRepository managementSystemRepository) {
            Member freePerson = managementSystemRepository.findPersonByName(personName);
            Team team = managementSystemRepository.findTeamByName(teamName);
            managementSystemRepository.addPersonToTeam(freePerson, team);
            return format(MEMBER_SUCCESS_MESSAGE, personName, teamName);
        }
    }
}




