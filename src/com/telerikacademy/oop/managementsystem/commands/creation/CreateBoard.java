package com.telerikacademy.oop.managementsystem.commands.creation;

import an.awesome.pipelinr.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.models.CommandResult;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.models.dtos.BoardDto;

import java.util.List;

import static java.lang.String.format;

public class CreateBoard {
    public static class CommandImpl implements Command<CommandResult> {
        public final ManagementSystemRepository managementSystemRepository;
        public final List<String> parameters;
        public BoardDto boardDto;

        public CommandImpl(ManagementSystemRepository managementSystemRepository, List<String> parameters) {
            this.managementSystemRepository = managementSystemRepository;
            this.parameters = parameters;
        }
    }

    public static class CommandHandler implements Command.Handler<CommandImpl, CommandResult> {

        private static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board \"%s\" in team \"%s\" created.";

        @Override
        public CommandResult handle(CommandImpl request) {
            String message = createBoard(request.boardDto.boardName(), request.boardDto.teamName(), request.managementSystemRepository);
            return new CommandResult(message);
        }

        private String createBoard(String boardName, String teamName, ManagementSystemRepository managementSystemRepository) {
            Board boardToBeAdded = managementSystemRepository.createBoard(boardName);
            Team team = managementSystemRepository.findTeamByName(teamName);
            team.addBoard(boardToBeAdded);
            return format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, teamName);
        }
    }
}
