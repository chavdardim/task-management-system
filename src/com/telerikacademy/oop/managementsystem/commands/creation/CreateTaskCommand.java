package com.telerikacademy.oop.managementsystem.commands.creation;

import com.telerikacademy.oop.managementsystem.commands.contracts.Command;
import com.telerikacademy.oop.managementsystem.core.contracts.ManagementSystemRepository;
import com.telerikacademy.oop.managementsystem.exceptions.ElementNotFoundException;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Board;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Task;
import com.telerikacademy.oop.managementsystem.models.contracts.models.Team;
import com.telerikacademy.oop.managementsystem.models.enums.TaskType;
import com.telerikacademy.oop.managementsystem.utils.ValidationHelpers;

import java.util.List;

import static com.telerikacademy.oop.managementsystem.utils.ParsingHelpers.tryParseEnum;
import static java.lang.String.format;

public class CreateTaskCommand implements Command {
    private static final String TASK_CREATED_SUCCESS_MESSAGE = "%s in the %s board of %s with ID %d is created.";
    public static final int EXPECTED_NUMBER_OF_ARGUMENTS = 5;

    private final ManagementSystemRepository managementSystemRepository;

    public CreateTaskCommand(ManagementSystemRepository managementSystemRepository) {
        this.managementSystemRepository = managementSystemRepository;
    }

    @Override
    public String execute(List<String> arguments) {
        ValidationHelpers.validateArgumentsCount(arguments, EXPECTED_NUMBER_OF_ARGUMENTS);
        String title = arguments.get(0);
        String description = arguments.get(1);
        String teamName = arguments.get(2);
        String boardName = arguments.get(3);
        TaskType taskType = tryParseEnum(arguments.get(4), TaskType.class);
        return createTask(title, description, teamName, boardName, taskType);
    }

    private String createTask(String title, String description, String teamName, String boardName, TaskType taskType) {
        Team team = managementSystemRepository.findTeamByName(teamName);
        Board board = team.getBoards().stream()
                .filter(b -> b.getName().equals(boardName))
                .findFirst()
                .orElseThrow(() -> new ElementNotFoundException(format(
                        "Board with name %s does not exist in team %s.", boardName, teamName)));

        int nextFreeId = managementSystemRepository.getAllTasks().size() + 1;
        Task task = taskType.createInstance(nextFreeId, title, description);
        board.addTask(task);
        return format(TASK_CREATED_SUCCESS_MESSAGE, taskType, boardName, teamName, nextFreeId);
    }
}
