package com.telerikacademy.oop.managementsystem.commands.enums;

public enum CommandType {

    ASSIGNALLBUGS,
    ASSIGNALLSTORIES,
    ASSIGN,
    UNASSIGN,

    CHANGEBUG,
    CHANGEFEEDBACK,
    CHANGESTORY,

    ADDCOMMENT,
    ADDMEMBER,
    ADDREPRODUCINGSTEPS,
    CREATEBOARD,
    CREATEPERSON,
    CREATEPERSON2,
    CREATETASK,
    CREATETEAM,

    FILTERALLTASKSBYASSIGNEE,
    FILTERALLTASKSBYSTATUSANDASSIGNEE,
    FILTERALLTASKSBYSTATUS,
    FILTERALLTASKSBYTITLEKEYWORD,
    FILTERALLTASKSWITHOUTASSIGNEE,
    FILTERTASKTYPEBYASSIGNEE,
    FILTERTASKTYPEBYSTATUS,

    SHOWACTIVITY,
    SHOWCOMMENTS,
    SHOWPEOPLE,
    SHOWTEAMMEMBERS,

    SORTALLASSIGNEDTASKSBYTITLE,
    SORTALLTASKSBYTITLE,
    SORTTASKTYPEBYPRIORITY,
    SORTTASKTYPEBYRATING,
    SORTTASKTYPEBYSEVERITY,
    SORTTASKTYPEBYSIZE,
    SORTTASKTYPEBYTITLE,




















    /*
    Operations
The application must support the following operations:

Create a new person.                                CREATEPERSON
Show all people.                                    SHOWPEOPLE
Show person's activity.                             SHOWACTIVITY
Create a new team.                                  CREATETEAM
Show all teams.                                     SHOWTEAMS
Show team's activity.                               SHOWACTIVITY
Add person to team.                                 ADDMEMBER
Show all team members.                              SHOWMEMBERS
Create a new board in a team.                       CREATEBOARD
Show all team boards.                               SHOWBOARDS
Show board's activity.                              SHOWACTIVITY
Create a new Bug/Story/Feedback in a board.         CREATETASK (CREATEBUG, CREATESTORY, CREATEFEEDBACK)
Change the Priority/Severity/Status of a bug.       CHANGEPRIORITY, CHANGESEVERITY, CHANGESTATUS
Change the Priority/Size/Status of a story.         CHANGEPRIORITY, CHANGESIZE,     CHANGESTATUS
Change the Rating/Status of a feedback.             CHANGERATING,                   CHANGESTATUS
Assign/Unassign a task to a person.                 ASSIGN,         UNASSIGN
Add comment to a task.                              ADDCOMMENT
     */
}
