package com.telerikacademy.oop.managementsystem.commands.contracts;

import java.util.List;

public interface Command {

    String execute(List<String> arguments);

}
