<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

<p><img src="https://i.ibb.co/SVyKj2C/SCHEME.jpg" alt="SCHEME" border="0"></p>

###OOP Teamwork
in colaboration of Ivaylo Stavrev & Chavdar Dimitrov
#Task Management System

###Project Description

Design and implement a **Tasks Management** console application.
The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.
---
###Assign

`AssignAllBugs [members] [bugFeature*] [featureType*]` - Assign all bug of chosen feature type (ex. AssignAllBugs Gosho Priority High).

    - bugFeature can be one of the following - Priority || Severity || BugStatus
    - featureType can be one of the following 
        - Priority - Low || Medium || High
        - Severity - Minor || Major || Critical
        - BugStatus - Active || Fixed
    - If bugs are already assigned - IllegalArgumentException is thrown
    - If there are no bugs - IllegalArgumentException is thrown
    - If bugFeature is mistaken - InvalidUserInputException is thrown

`AssignAllStories [members] [storyFeature*] [featureType*]` - Assign all stories of chosen feature type (ex. AssignAllStories Gosho Priority High).

    - bugFeature can be one of the following - Priority || Size || StoryStatus
    - featureType can be one of the following 
        - Priority - Low || Medium || High
        - Size - Small || Major || Large
        - StoryStatus - NotDone || InProgress || Done
    - If stories are already assigned - IllegalArgumentException is thrown
    - If there are no stories - IllegalArgumentException is thrown
    - If storyFeature is mistaken - InvalidUserInputException is thrown

`Assign [id] [members]` - Assign task to a member/s (ex. Assign 2 Gosho,Pesho).

    - If task is already assigned - InvalidUserInputException is thrown
    - If task is Feedback, which cannot be assigned - InvalidUserInputException is thrown
    - If member/s does not exist - ElementNotFoundException is thrown

`Unassign [id] [members]` - Unassign task to a member/s (ex. Unassign 2 Gosho,Pesho).

    - If task is already unassigned - InvalidUserInputException is thrown
    - If task is Feedback, which cannot be assigned - InvalidUserInputException is thrown
    - If member/s does not exist - ElementNotFoundException is thrown
---
###Change

`ChangeBug [id] [bugFeature*] [featureType*]` - Change feature of a bug (ex. ChangeBug 1 Priority High).

    - If no bug exists - InvalidUserInputException is thrown
    - If bugFeature is mistaken - InvalidUserInputException is thrown
    - If featureType is the same - InvalidUserInputException is thrown


`ChangeStory [id] [storyFeature*] [featureType*]` - Change feature of a story (ex. ChangeStory 1 Size Large).

    - If no story exists - InvalidUserInputException is thrown
    - If storyFeature is mistaken - InvalidUserInputException is thrown
    - If featureType is the same - InvalidUserInputException is thrown


`ChangeFeedback [id] [feedbackFeature*] [featureType*]` - Change feature of a feedback (ex. ChangeFeedback 1 Rating 3).

    - feedbackFeature can be Rating || FeedbackStatus
    - featureType in case of Rating is any number || in case of FeedbackStatus:
        - New || Unscheduled || Scheduled || Done
    - If no feedback exists - InvalidUserInputException is thrown
    - If feedbackFeature is mistaken - InvalidUserInputException is thrown
    - If featureType is the same - InvalidUserInputException is thrown
---
###Creation


`AddComment [id] [comment]` - Adds comment of a task (ex. AddComment 1 {{This is my first task}}).

    - If task doesn't exist - ElementNotFoundException is thrown
    - Comment should be between 10 & 50 symbols long or IllegalArgumentException will be thrown

`AddMember [personName] [teamName]` - Adds free person to team. Becomes member. (ex. AddMember Ivaylo Fathers).

    - If member of this team with the same name exists - InvalidUserInputException is thrown
    - If person doesn't exist - ElementNotFoundException is thrown
    - If team doesn't exist - ElementNotFoundException is thrown

`AddReproducingSteps [id] [reproducingSteps]` - Adds reproducing steps to a bug
 (ex. AddReproducingSteps 1 {{1. Open the application; 2. Click "Log In"; 3. The application freezes!}).

    - If bug doesn't exist - ElementNotFoundException is thrown
    - Reproducing steps should be between 10 & 100 symbols long or IllegalArgumentException will be thrown

`CreateBoard [boardName] [teamName]` - Creates a board in a team (ex. CreateBoard Chores Fathers).

    - Name should be between 5 and 10 symbols or IllegalArgumentException is thrown
    - If board already exists - InvalidUserInputException is thrown
    - If team doesn't exist - ElementNotFoundException is thrown

`CreatePerson [name]` - Creates a free person (ex. CreatePerson Chavdar).

    - Name should be between 5 and 15 symbols or IllegalArgumentException is thrown
    - If person already exist - InvalidUserInputException is thrown

`CreateTask [title] [description] [team] [board] [taskType] ` - Creates bug, story or feedback and adds it to the board of a team.

    - Title should be between 10 and 50 symbols or IllegalArgumentException is thrown
    - Description should be between 10 and 500 symbols or IllegalArgumentException is thrown
    - If team or board doesn't exist - ElementNotFoundException is thrown
    - taskType can be Bug || Story || Feedback

`CreateTeam [name]` - Creates team (ex. CreateTeam Fathers).

    - Name should be between 5 and 15 symbols or IllegalArgumentException is thrown
    - If team already exist - InvalidUserInputException is thrown
---
###Filter

`FilterAllTasksByAssignee [members]` - Filter all assigned tasks by chosen member or list of members separated by comma (ex. FilterAllTasksByAssignee Pesho). 

    - If there is no such member name, an ElementNotFoundException is thrown.
    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterAllTasksByStatusAndAssignee [members] [status]` - Filter all assigned tasks by chosen member or list of members separated by comma and by chosen status (ex. FilterAllTasksByAssignee Pesho Active).

    - Status could be any of the possible assignable statuses - Active || Fixed || NotDone || InProgress || Done.
    - If there is no such member name, an ElementNotFoundException is thrown.
    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterAllTasksByStatus [status]` - Filter all tasks by chosen status (ex. FilterAllTasksByAssignee Active). 

    - Status could be any of the possible statuses:
        - Active || Fixed || NotDone || InProgress || New || Unscheduled || Scheduled || Done.
    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterAllTasksByTitleKeyword [keyword]` - Filter all tasks by title that contains chosen keyword (ex. FilterAllTasksByTitleKeyword freezes).

    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterAllTasksWithoutAssignee` - Filter all tasks that has been assigned.

    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterTaskTypeByAssignee [taskType] [member]` - Filter assigned task by chosen member(ex.FilterTaskTypeByAssignee Bug Pesho.
    
    - Task type could be either Bug or Story.
    - If entered another task type, an InvalidUserInputException is thrown.
    - If there is no such member name, an ElementNotFoundException is thrown.
    - If there are no tasks, an IllegalArgumentException is thrown.

`FilterTaskTypeByStatusAndAssignee [member] [taskType] [status]` - Filter assigned task by chosen member, chosen type (Bug or Story) and chosen status (ex. FilterTaskTypeByStatusAndAssignee Pesho Bug Active). Status should be one of the chosen task type's corresponding statuses.

    - If there is no such member name, an ElementNotFoundException is thrown.
    - If entered another task type, an InvalidUserInputException is thrown.
    - If entered invalid status, an IllegalArgumentException is thrown.

`FilterTaskTypeByStatus [taskType] [status]` - Filter tasks by chosen type (Bug, Story or Feedback) and chosen status (ex. FilterTaskTypeByStatus Bug Active). Status should be one of the chosen task type's corresponding statuses.

    - If entered another task type, an InvalidUserInputException is thrown.
    - If entered invalid status, an IllegalArgumentException is thrown.
---
###Show

`ShowPeople` - Show all people who do not belong to any team.

    - If there are no such people, an IllegalArgumentException is thrown. 

`ShowTeamMembers [teamName]` - Show all members of the chosen team.

    - If there is no such team or team has no members, an IllegalArgumentException is thrown.

`ShowComments [id]` - Show all comments of task with specified ID.

    - If ID is not a valid number, an IllegalArgumentException is thrown.
    - If there is no task with specified ID, an ElementNotFoundException is thrown.

`ShowActivity [names]` - Show activity of member, team, board by specified name or names separated by comma (ex. [ShowActivity Pesho] or [ShowActivity Pesho,Team1,Board3]).
    
    - If there is no member, team or board with specified name, an ElementNotFoundException is thrown.
---
###Sort

`SortAllAssignedTasksByTitle` - Sort and list all assigned tasks by title.

`SortAllTasksByTitle` - Sort and list all tasks by title.

`SortTaskTypeByPriority [taskType]` - Sort and list tasks of chosen type by priority.

    - If there is no such type, an IllegalArgumentException is thrown.
    - If there is no task of specified type, an ElementNotFoundException is thrown.

`SortTaskTypeByRating` - Sort and list all tasks by rating. Currently, only feedback have rating.

    - If there is no feedback, an ElementNotFoundException is thrown.

`SortTaskTypeBySeverity` - Sort and list all tasks by severity. Currently, only bugs have severity.

    - If there is no bug, an ElementNotFoundException is thrown.

`SortTaskTypeBySize` - Sort and list all tasks by size. Currently, only stories have size.

    - If there is no story, an ElementNotFoundException is thrown.

`SortTaskTypeByTitle [taskType]` - Sort and list all tasks of chosen type by title.

    - If there is no such type of tasks, an IllegalArgumentException is thrown.
    - If there is no task of chosen type, an ElementNotFoundException is thrown.
---







