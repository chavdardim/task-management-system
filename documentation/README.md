<img src="https://webassets.telerikacademy.com/images/default-source/logos/telerik-academy.svg" alt="logo" width="300px" style="margin-top: 20px;"/>

###OOP Teamwork
in colaboration 
#Task Management System

###Project Description

Design and implement a **Tasks Management** console application.
The application will be used by a small team of developers, who need to keep track of all the tasks, surrounding a software product they are building.

###Functional Requirements

The application must support multiple **teams**. 

Each team must have a **name**, **members**, and **boards**.

- The name must be unique in the application.
- The name is a string between 5 and 15 symbols.

Each member must have a **name**, list of **tasks** and **activity history**.

- The name must be unique in the application.
- The name is a string between 5 and 15 symbols.

Each board must have a **name**, list of **tasks** and **activity history**.

- Name must be unique in the team.
- Name is a string between 5 and 10 symbols.

There are 3 types of tasks: **bug**, **story**, and **feedback**.

###Bug

Bugs must have an ID, a title, a description, a list of steps to reproduce it, a priority, a severity, a status, an assignee, a list of comments and a list of changes history.

- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Steps to reproduce is a list of strings.
- Priority is one of the following: **High**, **Medium**, or **Low**.
- Severity is one of the following: **Critical**, **Major**, or **Minor**.
- Status is one of the following: **Active** or **Fixed**.
- Assignee is a member from the team.
- Comments is a list of comments (string messages with an author).
- History is a list of all changes (string messages) that were done to the bug.

###Story

Stories must have an ID, a title, a description, a priority, a size, a status, an assignee, a list of comments and a list of changes history.

- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Priority is one of the following: **High**, **Medium**, or **Low**.
- Size is one of the following: **Large**, **Medium**, or **Small**.
- Status is one of the following: **Not Done**, **InProgress**, or **Done**.
- Assignee is a member from the team.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the story.

###Feedback

Feedbacks must have an ID, a title, a description, a rating, a status, a list of comments and a list of changes history.

- Title is a string between 10 and 50 symbols.
- Description is a string between 10 and 500 symbols.
- Rating is an integer.
- Status is one of the following: **New**, **Unscheduled**, **Scheduled**, or **Done**.
- Comments is a list of comments (string messages with author).
- History is a list of all changes (string messages) that were done to the feedback.

*Note: IDs of tasks must be unique in the application i.e., if we have a bug with ID 42 then we cannot have a story or a feedback with ID 42.*


###Operations

The application must support the following operations:
- Create a new person.
- Show all people.
- Show person's activity.
- Create a new team.
- Show all teams.
- Show team's activity.
- Add person to team.
- Show all team members.
- Create a new board in a team.
- Show all team boards.
- Show board's activity.
- Create a new Bug/Story/Feedback in a board.
- Change the Priority/Severity/Status of a bug.
- Change the Priority/Size/Status of a story.
- Change the Rating/Status of a feedback.
- Assign/Unassign a task to a person.
- Add comment to a task.

Filter

- FilterAllTasksByTitleKeyword 
- FilterTasktypeByStatus status
- FilterTasktypeByAssignee assignee tasktype / DONE
- FilterTasktypeByStatusAndAssignee assignee status // DONE
- FilterAllTasksByStatus status
- FilterAllTasksByAssignee assignee // DONE
- FitlerAllTasksByStatusAndAssignee assignee status

Sort

- SortAllTasksByTitle
- SortTaskTypeByTitle tasktype
- SortTaskTypeByPriority tasktype
- SortTaskTypeBySeverity tasktype
- SortTaskTypeBySize tasktype
- SortTaskTypeByRating tasktype
- SortAllAssignedTasksByTitle 

###Listing

List all tasks (display the most important info).

- Filter by title
- Sort by title

List bugs/stories/feedback only.

- Filter by status and/or assignee
- Sort by title/priority/severity/size/rating (depending on the task type)

List tasks with assignee.

- Filter by status and/or assignee
- Sort by title


##Use cases

###Use case #1

One of the developers has noticed a bug in the company's product. He starts the application and goes on to create a new Task for it. He creates a new Bug and gives it the title "The program freezes when the Log In button is clicked." For the description he adds "This needs to be fixed quickly!", he marks the Bug as High priority and gives it Critical severity. Since it is a new bug, it gets the Active status. The developer also assigns it to the senior developer in the team. To be able to fix the bug, the senior developer needs to know how to reproduce it, so the developer who logged the bug adds a list of steps to reproduce: "1. Open the application; 2. Click "Log In"; 3. The application freezes!" The bug is saved to the application and is ready to be fixed.

#### Sample input for Use case #1:

```
CreatePerson Ivaylo
CreatePerson Chavdar
CreateTeam Fathers
AddMember Ivaylo Fathers
AddMember Chavdar Fathers
CreateBoard Chores Fathers
CreateTask {{The program freezes when the LogIn btn is clicked.}} {{This needs to be fixed quickly!}} Fathers Chores Bug
AddReproducingSteps 1 {{1. Open the application; 2. Click "Log In"; 3. The application freezes!}}
exit

```

###Use case #2

A new developer has joined the team. One of the other developers starts the application and creates a new team member. After that, he adds the new team member to one of the existing teams and assigns all Critical bugs to him.

#### Sample input for Use case #2:

```none
CreatePerson Ivaylo
CreatePerson Chavdar
CreateTeam Fathers
AddMember Ivaylo Fathers
AddMember Chavdar Fathers
CreateBoard Chores Fathers
CreateTask {{The program freezes when the LogIn btn is clicked.}} {{This needs to be fixed quickly!}} Fathers Chores Bug
AddReproducingSteps 1 {{1. Open the application; 2. Click "Log In"; 3. The application freezes!}}
ChangeBug 1 Severity Critical
CreatePerson NewDeveloper
AddMember NewDeveloper Fathers
AssignAllBugs NewDeveloper Severity Critical
exit

```

###Use case #3

One of the developers has fixed a bug that was assigned to him. He adds a comment to that bug, saying "This one took me a while, but it is fixed now!", and then changes the status of the bug to Fixed. Just to be sure, he checks the changes history list of the bug and sees that the last entry in the list says, "The status of item with ID 42 was changed from Active to Done."

#### Sample input for Use case #3:

```none
CreatePerson Ivaylo
CreatePerson Chavdar
CreateTeam Fathers
AddMember Ivaylo Fathers
AddMember Chavdar Fathers
CreateBoard Chores Fathers
CreateTask {{The program freezes when the LogIn btn is clicked.}} {{This needs to be fixed quickly!}} Fathers Chores Bug
AddReproducingSteps 1 {{1. Open the application; 2. Click "Log In"; 3. The application freezes!}}
ChangeBug 1 Severity Critical
CreatePerson NewDeveloper
AddMember NewDeveloper Fathers
AssignAllBugs NewDeveloper Severity Critical
AddComment 1 {{This one took me a while, but it is fixed now!}}
ChangeBug 1 Status Fixed
FilterTaskTypeByStatus Bug Fixed
exit

```

### Sample Input

```none
CreatePerson Ivaylo
CreatePerson Chavdar
CreatePerson Toshko
CreatePerson Pesho
ShowPeople
CreateTeam Fathers
CreateTeam Trainers
AddMember Ivaylo Fathers
AddMember Chavdar Fathers
AddMember Toshko Trainers
AddMember Pesho Trainers
ShowTeamMembers Fathers
ShowTeamMembers Trainers
ShowPeople
CreateBoard Chores Fathers
CreateBoard T-board Trainers
CreateTask {{Christmas is coming}} {{Holidays are on their way}} Trainers T-board Story
AddComment 1 {{-Ooo, I am so excited. Can't wait to open the champagne.}}
CreateTask {{The program freezes when press the LogIn btn.}} {{This needs to be fixed quickly!}} Fathers Chores Bug
AddReproducingSteps 2 {{1. Open the application; 2. Click "Log In"; 3. The application freezes!}}
CreateTask {{The program behaves strangely.}} {{When mouse cursor aproaces LogIn button, the btn runs away randomly across the screen.}} Fathers Chores Bug
AddComment 1 {{-Yea, but drink carefully. Don't want heading, right?}}
CreateTask {{BringMeBeerCommand does not work.}} {{Just a half bottle left.}} Fathers Chores Bug
CreateTask {{Change the baby's diapers! You lazy bastard.}} {{Did you feel the smell?}} Fathers Chores Story
Assign 2 Chavdar,Ivaylo
Unassign 2 Ivaylo
Assign 5 Ivaylo
ChangeBug 2 Priority High
ChangeBug 2 Severity Critical
ChangeBug 3 Severity Critical
ChangeBug 4 Severity Critical
AddComment 1 {{-Who cares, lets become pigs. Cheers!}}
ShowComments 1
CreatePerson SantaClaus
AddMember SantaClaus Fathers
AssignAllBugs SantaClaus Severity Critical
FilterAllTasksByAssignee SantaClaus
AddComment 2 {{This one took me a while, but it is fixed now!}}
ChangeBug 2 Status Fixed
FilterTaskTypeByStatus Bug Fixed
SortAllTasksByTitle
FilterAllTasksByTitleKeyword Beer
exit

```

### Sample Output

```none
User Ivaylo created.
--------------------
User Chavdar created.
--------------------
User Toshko created.
--------------------
User Pesho created.
--------------------
People: 
1. Ivaylo
2. Chavdar
3. Toshko
4. Pesho
--------------------
Team Fathers created.
--------------------
Team Trainers created.
--------------------
Ivaylo is added to Fathers team
--------------------
Chavdar is added to Fathers team
--------------------
Toshko is added to Trainers team
--------------------
Pesho is added to Trainers team
--------------------
Members of Fathers team: 
1. Ivaylo
2. Chavdar
--------------------
Members of Trainers team: 
1. Toshko
2. Pesho
--------------------
There are no people without team.
--------------------
Board "Chores" in team "Fathers" created.
--------------------
Board "T-board" in team "Trainers" created.
--------------------
Story in the T-board board of Trainers with ID 1 is created.
--------------------
Story with ID 1 successfully commented.
--------------------
Bug in the Chores board of Fathers with ID 2 is created.
--------------------
Reproducing steps for Bug with ID 2 added.
--------------------
Bug in the Chores board of Fathers with ID 3 is created.
--------------------
Story with ID 1 successfully commented.
--------------------
Bug in the Chores board of Fathers with ID 4 is created.
--------------------
Story in the Chores board of Fathers with ID 5 is created.
--------------------
Bug with ID 2 assigned to [Chavdar, Ivaylo].
--------------------
Bug with ID 2 unassigned from [Ivaylo].
--------------------
Story with ID 5 assigned to [Ivaylo].
--------------------
Bug with ID 2 Priority changed to High.
--------------------
Bug with ID 2 Severity changed to Critical.
--------------------
Bug with ID 3 Severity changed to Critical.
--------------------
Bug with ID 4 Severity changed to Critical.
--------------------
Story with ID 1 successfully commented.
--------------------
--Task with ID 1 - COMMENTS--
-Ooo, I am so excited. Can't wait to open the champagne.
-Yea, but drink carefully. Don't want heading, right?
-Who cares, lets become pigs. Cheers!
--------------------
User SantaClaus created.
--------------------
SantaClaus is added to Fathers team
--------------------
All bugs with type CRITICAL are assigned to [SantaClaus].
--------------------
--All tasks filtered by ASSIGNEE SantaClaus--
Bug ID: 2
Title: The program freezes when press the LogIn btn.
Description: This needs to be fixed quickly!
Priority: HIGH
Severity: CRITICAL
Status: Active
Assignee: [Chavdar, SantaClaus]
Reproducing step: [1. Open the application; 2. Click "Log In"; 3. The application freezes!]
--Comments--
no comments
--History Log--
[29-December-2021 01:25:03] Bug with id 2 created.
[29-December-2021 01:25:03] Bug assigned to [Chavdar, Ivaylo].
[29-December-2021 01:25:03] Bug unassigned from [Ivaylo]
[29-December-2021 01:25:03] Bug priority changed from LOW to HIGH.
[29-December-2021 01:25:03] Bug severity changed from MINOR to CRITICAL.
[29-December-2021 01:25:03] Bug assigned to [SantaClaus].
--------------------
 Bug ID: 3
Title: The program behaves strangely.
Description: When mouse cursor aproaces LogIn button, the btn runs away randomly across the screen.
Priority: LOW
Severity: CRITICAL
Status: Active
Assignee: [SantaClaus]
Reproducing step: []
--Comments--
no comments
--History Log--
[29-December-2021 01:25:03] Bug with id 3 created.
[29-December-2021 01:25:03] Bug severity changed from MINOR to CRITICAL.
[29-December-2021 01:25:03] Bug assigned to [SantaClaus].
--------------------
 Bug ID: 4
Title: BringMeBeerCommand does not work.
Description: Just a half bottle left.
Priority: LOW
Severity: CRITICAL
Status: Active
Assignee: [SantaClaus]
Reproducing step: []
--Comments--
no comments
--History Log--
[29-December-2021 01:25:03] Bug with id 4 created.
[29-December-2021 01:25:03] Bug severity changed from MINOR to CRITICAL.
[29-December-2021 01:25:03] Bug assigned to [SantaClaus].
--------------------
Bug with ID 2 successfully commented.
--------------------
Bug with ID 2 Status changed to Fixed.
--------------------
--All Bugs filtered by Fixed--
Bug ID: 2
Title: The program freezes when press the LogIn btn.
Description: This needs to be fixed quickly!
Priority: HIGH
Severity: CRITICAL
Status: Fixed
Assignee: [Chavdar, SantaClaus]
Reproducing step: [1. Open the application; 2. Click "Log In"; 3. The application freezes!]
--Comments--
[This one took me a while, but it is fixed now!]
--History Log--
[29-December-2021 01:25:03] Bug with id 2 created.
[29-December-2021 01:25:03] Bug assigned to [Chavdar, Ivaylo].
[29-December-2021 01:25:03] Bug unassigned from [Ivaylo]
[29-December-2021 01:25:03] Bug priority changed from LOW to HIGH.
[29-December-2021 01:25:03] Bug severity changed from MINOR to CRITICAL.
[29-December-2021 01:25:03] Bug assigned to [SantaClaus].
[29-December-2021 01:25:03] Bug status changed from ACTIVE to FIXED.
--------------------
--Sorted by TITLE--
 Bug ID: 4
Title: BringMeBeerCommand does not work.
Description: Just a half bottle left.
Priority: LOW
Severity: CRITICAL
Status: Active
Assignee: [SantaClaus]
Reproducing step: []
 Story ID: 5
Title: Change the baby's diapers! You lazy bastard.
Description: Did you feel the smell?
Priority: LOW
Size: SMALL
Status: Notdone
Assignee: [Ivaylo]
 Story ID: 1
Title: Christmas is coming
Description: Holidays are on their way
Priority: LOW
Size: SMALL
Status: Notdone
Assignee: []
 Bug ID: 3
Title: The program behaves strangely.
Description: When mouse cursor aproaces LogIn button, the btn runs away randomly across the screen.
Priority: LOW
Severity: CRITICAL
Status: Active
Assignee: [SantaClaus]
Reproducing step: []
 Bug ID: 2
Title: The program freezes when press the LogIn btn.
Description: This needs to be fixed quickly!
Priority: HIGH
Severity: CRITICAL
Status: FIXED
Assignee: [Chavdar, SantaClaus]
Reproducing step: [1. Open the application; 2. Click "Log In"; 3. The application freezes!]
--------------------
--All tasks filtered by "Beer" keyword--
 Bug ID: 4
Title: BringMeBeerCommand does not work.
Description: Just a half bottle left.
Priority: LOW
Severity: CRITICAL
Status: Active
Assignee: [SantaClaus]
Reproducing step: []
--------------------

```

> *Hint:* Don't use the whole input as a test. Break it as simple as possible. Maybe one line at a time is a good starting point.


##Technical Requirements

- Follow the **OOP best practices**:

  - Use data encapsulation.
  - Use inheritance and polymorphism properly.
  - Use interfaces and abstract classes properly.
  - Use static members properly.
  - Use enumerations properly.
  - Aim for strong cohesion and loose coupling.
  

- Follow guidelines for writing **clean code**:
  - Proper naming of classes, methods, and fields.
  - Small classes and methods.
  - Well formatted and consistent code.
  - No duplicate code.


- Implement proper user input **validation** and display meaningful user messages.
- Implement proper **exception handling**.
- Prefer using the Streaming API, then using ordinary loops, wherever you can.
- Cover the core functionality with unit tests (**at least 80%** code coverage of the models and commands).
- Use **Git** to keep your source code and for team collaboration.

##Teamwork Guidelines
Please see the Teamwork Guidelines document.

##Project Defense
Each team will have a defense of their project with the trainers where they must explain the application structure, major architectural components and selected source code pieces demonstrating the implementation of key features. 

Prepare a list of commands to demonstrate how the program works.

##Expectations

You must understand the system you have created.

It is OK if your application has flaws or is missing a couple of must's. What's not OK is if you do not know what is working and what is not. That said, you should be aware of any defects or incomplete functionality must be properly documented and secured.

Some things you need to be able to explain during your project defense:

- What are the most important things you have learned while working on this project?
- What are the worst "hacks" in the project, or where do you think it needs improvement?
- What more would you do if you had another week to work on the system?
- What would you do differently if you were implementing the system again?




